#pragma once

#include <cinttypes>

namespace RRR_FMIndex {
    namespace Samples {
        /* ��������� �������� ������ � SuffixArray ����� N ������ � ����� h = SAMPLE_DISTANCE.
        � i-�� ���� ������ �������� i+1-�� ����.
        ������� ����� ����� �����: h, 2h, ..., (k-1)h, min(kh, N-1).
        �.�. ������� �������� ������� �������� �� � 0-�� �������, � � h-��.
        N = n+1, ��� n - ����� �������� ������, � n+1 - ����� ������, � ����������� � �����
        �������� $, ��� ������� Suffix Array � ��� ��������. */
        struct InversedSuffixArraySamples {
            static constexpr uint64_t SAMPLE_DISTANCE = 256;

            uint64_t* samples = nullptr;
            uint64_t samplesCount;
            uint64_t lastSample;

            void Initialize(const uint64_t* SAinv, const uint64_t N) {
                lastSample = uint64_t(N - 1);
                samplesCount = uint64_t((N - 1) / SAMPLE_DISTANCE);

                if (lastSample - samplesCount * SAMPLE_DISTANCE > 1) {
                    samplesCount++;
                }

                samples = new uint64_t[samplesCount];

                uint64_t nextIdx = 0;
                uint64_t i = 0;
                while (i < samplesCount) {
                    nextIdx += SAMPLE_DISTANCE;
                    nextIdx = nextIdx >= N - 1 ? N - 2 : nextIdx;

                    samples[i] = SAinv[nextIdx + 1];
                    i++;
                }
            }

            void Deinitialize() {
                if (samples) {
                    delete[] samples;
                }
            }

            /* ���������� �������� ���� �����, ���������� ����� � ��������� x.
            x = 0, 1, ..., N - 1 */
            uint64_t GetNearestSample(uint64_t i) const {
                uint64_t nearestIdx = i / SAMPLE_DISTANCE;
                if (i != 0 && i % SAMPLE_DISTANCE == 0) {
                    nearestIdx--;
                }

                return samples[nearestIdx];
            }

            /* ���������� ���������� �� ���������� � i ����� ���� �����.
            i = 0, 1, ..., N-1 */
            uint64_t GetDistanceToNearestSample(uint64_t i) const {
                // ��� ��������, ��� ������ � ��������� ����, ��� �������� ��������� �����
                if (i / SAMPLE_DISTANCE == samplesCount - 1 && i % SAMPLE_DISTANCE != 0) {
                    return lastSample - i - 1;
                }

                if (i % SAMPLE_DISTANCE == 0 && i != 0) {
                    return 0;
                }

                if (i == 0 && lastSample <= SAMPLE_DISTANCE) {
                    return lastSample - 1;
                }

                return (SAMPLE_DISTANCE - (i % SAMPLE_DISTANCE));
            }

            double SizeMB() const {
                return sizeof(uint64_t) * samplesCount / 1048576.0;
            }
        };
    }
}