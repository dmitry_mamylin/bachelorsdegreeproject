﻿#pragma once

#include <cstdint>

#include "../WildcardedString.h"

namespace RRR_FMIndex {
    namespace BWT {
        class BWT {
        public:
            class iterator;

            BWT(const Utility::WildcardedString& string, const uint64_t* suffixArray) :
                SA(suffixArray),
                str(string) {}

            char operator[](const size_t i) const {
                uint64_t saValue = SA[i];

                if (saValue == 0) {
                    return WILDCARD;
                } else {
                    return str[saValue - 1];
                }
            }

            bool IsWildcard(char c) const {
                return str.IsWildcard(c);
            }

            size_t length() const {
                return str.length();
            }

            iterator begin() const {
                return iterator(*this, 0);
            }

            iterator end() const {
                return iterator(*this, str.length());
            }

            class iterator {
            public:
                iterator(const BWT& _bwt, size_t position) :
                    bwt(_bwt),
                    pos(position) {}

                bool operator!=(const iterator& other) const {
                    return pos != other.pos;
                }

                const iterator& operator++() {
                    ++pos;
                    return *this;
                }

                char operator*() const {
                    return bwt[pos];
                }

            private:
                const BWT& bwt;
                size_t pos;
            };

        private:
            static constexpr char WILDCARD = char(1);

            const uint64_t* SA;
            const Utility::WildcardedString& str;
        };
    }
}