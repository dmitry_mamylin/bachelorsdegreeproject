﻿#pragma once

#include "BWT.h"
#include "InversedSuffixArraySamples.h"
#include "SuffixArraySamples.h"

#include "../WaveletTree/WaveletTree.h"
#include "../SuffixArrayUtilities.h"
#include "../WildcardedString.h"

#include <string>
#include <cstdint>
#include <vector>
#include <fstream>

namespace RRR_FMIndex {
    // Символ char(0) не используется, а char(1) считается уникальным, т.е. '$'
    class RRR_FMIndex {
    public:
        RRR_FMIndex(const std::string& str) {
            uint64_t* SA; // SuffixAray
            uint64_t* invSA; // inversedSuffixArray
            Utility::WildcardedString wdStr(str, str.length() + 1);

            SA = new uint64_t[2 * (str.length() + 1)];
            invSA = &SA[str.length() + 1];

            Utility::CreateSuffixArray(wdStr, SA, invSA);
            BWT::BWT bwt(wdStr, SA);

            /* На всякий случай переформируем массив invSA (если менять алгоритм построения
            SA на тот, что O(n), то это понадобится)*/
            for (uint64_t i = 0; i < str.length() + 1; i++) {
                invSA[SA[i]] = i;
            }

            wtree = new WaveletTree::WaveletTree(bwt);

            ComputeCFunction(str);

            this->invSA.Initialize(invSA, wdStr.length());
            this->SA.Initialize(SA, invSA, wdStr.length());

            delete[] SA;
        }

        ~RRR_FMIndex() {
            if (wtree) {
                delete wtree;
            }

            invSA.Deinitialize();
            SA.Deinitialize();
        }

        // Возвращает длину исходной строки
        uint64_t Length() const {
            return wtree->Length() - 1;
        }

        double SizeMB() const {
            return wtree->TotalSizeMB() +
                SA.SizeMB() +
                invSA.SizeMB() +
                sizeof(C) / 1048576.0;
        }

        // Возвращает подстроку текста T[s..e], но в обратном порядке
        std::string ReadSegment(uint64_t s, uint64_t e) const {
            std::string segment;

            uint64_t bitsToRead = e - s + 1;
            segment.reserve(size_t(bitsToRead));

            uint64_t nearestSample = invSA.GetNearestSample(e);
            uint64_t distance = invSA.GetDistanceToNearestSample(e);

            while (distance > 0) {
                nearestSample = LF(nearestSample);
                distance--;
            }

            while (bitsToRead > 0) {
                segment += (*wtree)[nearestSample];
                bitsToRead--;
                nearestSample = LF(nearestSample);
            }

            return segment;
        }

        // Сохраняет в storage подстроку текста T[s..e], но в обратном порядке; s,e = 0, 1, ..., n
        void ReadSegment(std::string& storage, uint64_t s, uint64_t e) const {
            uint64_t bitsToRead = e - s + 1;

            uint64_t nearestSample = invSA.GetNearestSample(e);
            uint64_t distance = invSA.GetDistanceToNearestSample(e);

            while (distance > 0) {
                nearestSample = LF(nearestSample);
                distance--;
            }

            while (bitsToRead > 0) {
                storage += (*wtree)[nearestSample];
                bitsToRead--;
                nearestSample = LF(nearestSample);
            }
        }

        // Возвращает количество вхождений паттерна pat в строку
        uint64_t OccurencesCount(const std::string& pat) const {
            uint64_t s, e;
            ComputeOccInterval(pat, s, e);
            return e < s ? 0 : e - s + 1;
        }

        /* Находит все вхождения паттерна pat в текст и сохраняет их в occs.
        Возвращает количество вхождений */
        uint64_t LocateOccurences(const std::string& pat, std::vector<uint64_t>& occs) const {
            uint64_t s, e;
            ComputeOccInterval(pat, s, e);

            if (e < s) {
                return 0;
            }

            for (uint64_t i = s; i <= e; i++) {
                uint64_t stepsCount = 0;
                uint64_t pos = i;

                while (!SA.IsSample(pos)) {
                    pos = LF(uint64_t(pos));
                    stepsCount++;
                }

                occs.push_back(SA[pos] + stepsCount);
            }

            return e - s + 1;
        }

    private:
        WaveletTree::WaveletTree* wtree = nullptr;
        Samples::InversedSuffixArraySamples invSA;
        Samples::SuffixArraySamples SA;
        uint64_t C[256]; // С[c] - количество символов, входящих в строку, которые меньше c

        // i = 0, 1, ..., n
        uint64_t LF(uint64_t i) const {
            uint8_t c;
            uint64_t rank;

            wtree->GetRankAndSymbol(i, rank, c);
            return C[c] + rank - 1;

            /*char Li = (*wtree)[i];
            uint8_t c = *reinterpret_cast<uint8_t*>(&Li);
            return C[c] + wtree->Rank(c, i) - 1;*/
        }

        // Заполняет массив C
        void ComputeCFunction(const std::string& str) {
            uint64_t occurencesCount[256];

            memset(C, 0, sizeof(C));
            memset(occurencesCount, 0, sizeof(occurencesCount));

            for (size_t i = 0; i < str.length(); i++) {
                occurencesCount[*reinterpret_cast<const uint8_t*>(&str[i])]++;
            }

            occurencesCount[1] = 1; // Устанавливаем вхождение для $

            for (uint32_t i = 2; i < 256; i++) {
                if (occurencesCount[i] > 0) {
                    for (uint32_t j = 1; j < i; j++) {
                        C[i] += occurencesCount[j];
                    }
                }
            }
        }

        /* Вычисляет интервалы вхождений строки pat в матрицу BWM.
        Сохраняет начало в s, а конец в e. Если e < s, то вхождений нет.
        s, e = 0, 1, ..., n */
        void ComputeOccInterval(const std::string& pat, uint64_t& s, uint64_t& e) const {
            uint8_t c = *reinterpret_cast<const uint8_t*>(&pat[pat.length() - 1]);
            s = C[c];
            e = C[c] + wtree->Rank(c, Length()) - 1;

            for (int64_t i = pat.length() - 2; i >= 0 && s <= e; i--) {
                c = *reinterpret_cast<const uint8_t*>(&pat[i]);
                s = C[c] + wtree->Rank(c, s - 1);
                e = C[c] + wtree->Rank(c, e) - 1;
            }
        }
    };
}