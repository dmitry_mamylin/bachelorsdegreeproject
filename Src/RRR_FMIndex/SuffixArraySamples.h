#pragma once

#include <cstdint>
#include <cmath>
#include <algorithm>
#include <vector>

#include "../RRRSuccinctBinaryString/RRRSuccinctBinaryString.h"
#include "../BitField.h"

namespace RRR_FMIndex {
    namespace Samples {
        /* ��������� ���������� ������ SA ������ � ����� SAMPLE_DISTANCE, � ����� �������
        (x = 0, PART_SIZE, 2*PART_SIZE, ... - ���� �����) ������ �������� SA[x] */
        struct SuffixArraySamples {
            static constexpr uint32_t SAMPLE_DISTANCE = 32;
            //static constexpr uint32_t SAMPLE_DISTANCE = 4;

            // ��� �� ������, ������ ��� ������ ������� -1 � samples[0]
            uint64_t* samples = nullptr;
            RRR::RRRSuccinctBinaryString* sampleBits = nullptr;
            uint64_t samplesCount;

            /* SA - ���������� ������ ����� N */
            void Initialize(const uint64_t* SA, const uint64_t* invSA, const uint64_t N) {
                Utility::BitField sampleFlags;
                samplesCount = uint64_t(ceil(double(N) / SAMPLE_DISTANCE));
                samples = new uint64_t[samplesCount];
                std::vector<uint64_t> invSaSamples;

                sampleFlags.AllocateZeros(N);
                invSaSamples.reserve(N);

                for (uint64_t suffix = 0; suffix < N; suffix += SAMPLE_DISTANCE) {
                    //samples[suffix / SAMPLE_DISTANCE] = suffix;
                    sampleFlags.WriteBit(invSA[suffix], 1);
                    invSaSamples.push_back(invSA[suffix]);
                }

                /*std::sort(invSaSamples.begin(), invSaSamples.end());
                uint64_t suffixIdx = 0;
                for (uint64_t suffixPos : invSaSamples) {
                    samples[suffixIdx] = SA[suffixPos];
                    suffixIdx++;
                }*/
                uint64_t suffixIdx = 0;
                for (uint64_t suffixPos = 0; suffixPos < N; suffixPos++) {
                    if (sampleFlags[suffixPos] == 1) {
                        samples[suffixIdx] = SA[suffixPos];
                        suffixIdx++;
                    }
                }

                sampleBits = new RRR::RRRSuccinctBinaryString(sampleFlags);
            }

            void Deinitialize() {
                if (samples) {
                    delete[] samples;
                }

                if (sampleBits) {
                    delete sampleBits;
                }
            }

            double SizeMB() const {
                return sampleBits->TotalSizeMB() +
                    (sizeof(uint64_t) * samplesCount / 1048576.0);
            }

            uint64_t operator[](const uint64_t i) const {
                return samples[sampleBits->Rank1(i) - 1];
                //return (sampleBits->Rank1(i) - 1) * SAMPLE_DISTANCE;
            }

            bool IsSample(uint64_t x) const {
                //return x % SAMPLE_DISTANCE == 0;
                return (*sampleBits)[x] == 1;
            }
        };
    }
}