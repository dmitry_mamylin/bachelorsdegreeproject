﻿#pragma once

#include <stdlib.h>
#include <string.h>
#include <cinttypes>

namespace Utility {
    class BitField {
    private:
        uint8_t* mBlocks; // Массив байтов
        uint64_t mFreeBitIdx; // Первый свободный бит
        uint64_t mSize; // Количество выделенных байтов (длина массива mBlocks)

        // Нумерация битов слева направо
        uint8_t GetBitFromBlock(uint64_t blockIdx, uint8_t bitIdx) const {
            return (mBlocks[blockIdx] >> (7 - bitIdx)) & uint8_t(1);
        }

    public:
        BitField() {
            mBlocks = (uint8_t*)malloc(sizeof(uint8_t));
            memset((void*)mBlocks, 0, sizeof(uint8_t));
            mFreeBitIdx = 0;
            mSize = 1;
        }

        /* Выделяет блок памяти размером bytes байтов, не меняя его содержимое */
        BitField(const uint64_t bytes) {
            mBlocks = (uint8_t*)malloc(bytes);
            mFreeBitIdx = bytes * 8;
            mSize = bytes;
        }

        BitField(const BitField& other) {
            mBlocks = (uint8_t*)malloc(other.mSize);
            memcpy((void*)mBlocks, (void*)other.mBlocks, other.mSize);
            mFreeBitIdx = other.mFreeBitIdx;
            mSize = other.mSize;
        }

        BitField(BitField&& other) {
            mBlocks = other.mBlocks;
            other.mBlocks = nullptr;
            mFreeBitIdx = other.mFreeBitIdx;
            mSize = other.mSize;
        }

        ~BitField() {
            free((void*)mBlocks);
        }

        uint8_t* Data() const {
            return mBlocks;
        }

        /* Выделяет столько байт памяти, сколько достаточно, чтобы сохранить хотя бы minBitSize битов/
        При этом, BitField считается пустым */
        void Allocate(uint64_t minBitSize) {
            uint64_t blocksCount = 1 + minBitSize / 8;
            uint64_t byteSize = sizeof(uint8_t) * blocksCount;

            mBlocks = (uint8_t*)realloc((void*)mBlocks, byteSize);
            memset((void*)mBlocks, 0, byteSize);
            mFreeBitIdx = 0;
            mSize = blocksCount;
        }

        /* Выделяет память для хранения minBitSize битов, заполняет их нулями делает размер BitField'а
        равным minBitSize */
        void AllocateZeros(uint64_t minBitSize) {
            uint64_t blocksCount = 1 + minBitSize / (8 * sizeof(uint8_t));
            uint64_t byteSize = sizeof(uint8_t) * blocksCount;

            mBlocks = (uint8_t*)realloc((void*)mBlocks, byteSize);
            memset((void*)mBlocks, 0, byteSize);
            mFreeBitIdx = minBitSize;
            mSize = blocksCount;
        }

        void AddBit(uint8_t bit) {
            uint64_t blockIdx = mFreeBitIdx / 8;
            uint64_t position = mFreeBitIdx % 8;
            uint64_t shift = 7 - position;
            uint8_t mask = ~(uint8_t(1) << shift);

            mBlocks[blockIdx] &= mask;
            mBlocks[blockIdx] |= (bit & uint8_t(1)) << shift;
            mFreeBitIdx++;
        }

        void AddByteUnsafe(uint8_t byte) {
            mBlocks[mFreeBitIdx / 8] = byte;
            mFreeBitIdx += 8;
        }

        uint64_t Length() const {
            return mFreeBitIdx;
        }

        uint64_t BytesCount() const {
            return mFreeBitIdx / 8 + (mFreeBitIdx % 8 == 0 ? 0 : 1);
        }

        /* idx = 0, 1, ..., Length()-1 */
        uint8_t ReadBit(uint64_t idx) const {
            uint64_t blockSize = 8 * sizeof(uint8_t);
            uint64_t blockIdx = idx / blockSize;
            uint64_t bitIdx = idx % blockSize;

            return GetBitFromBlock(blockIdx, (uint8_t)bitIdx);
        }

        /* idx = 0, 1, ..., Length()-1 */
        void WriteBit(uint64_t idx, uint8_t bit) {
            uint64_t blockIdx = idx / 8;
            uint64_t bitIdx = idx % 8;
            uint8_t block = mBlocks[blockIdx];

            uint8_t mask = uint8_t(0xff) ^ (uint8_t(1) << (7 - bitIdx));

            bit = (bit & (uint8_t(1))) << (7 - bitIdx);
            block = bit | (block & mask);

            mBlocks[blockIdx] = block;
        }

        /* idx = 0, 1, ..., Length()/8 */
        uint8_t ReadByte(uint64_t idx) const {
            return mBlocks[idx];
        }

        /* idx = 0, 1, ..., Length()-1 */
        uint8_t operator[](uint64_t idx) const {
            return ReadBit(idx);
        }
    };
}