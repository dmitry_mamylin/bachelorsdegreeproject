﻿#pragma once

#include <cstdint>
#include <cmath>

namespace Utility {
    template<class TYPE, uint64_t SAMPLE_STEP >
    struct Samples {
        /* Возвращает количество байтов, необходимое для хранения массива samples,
        разбивающего rangeToSample на сэмплы с шагом SAMPLE_STEP */
        uint64_t SizeInBytes(uint64_t rangeToSample) const {
            uint64_t bytesPerSample = sizeof(TYPE);
            return bytesPerSample * SamplesCount(rangeToSample);
        }

        // data - уже выделенная память размером SizeInBytes(), осталось ее разметить
        void LinkMemory(void* data) {
            samples = (TYPE*)data;
        }

        uint64_t SamplesCount(uint64_t rangeToSample) const {
            return uint64_t(ceil(double(rangeToSample) / SAMPLE_STEP));
        }

        TYPE& operator[](const uint64_t i) {
            return samples[i];
        }

        TYPE operator[](const uint64_t i) const {
            return samples[i];
        }

    private:
        TYPE* samples = nullptr;
    };
}