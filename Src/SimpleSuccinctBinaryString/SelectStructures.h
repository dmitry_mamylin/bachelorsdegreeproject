﻿#pragma once

#include <cinttypes>
#include <algorithm>
#include <vector>

#include "../BitField.h"

// В общем случае: log^2(n)
constexpr uint32_t SELECT_BLOCK_SIZE = 256;
// Если блок покрывает больше, чем SELECT_LONG_BLOCK_THRESHOLD, то он - long-блок. В общем случае: log^4(n)
constexpr uint32_t SELECT_LONG_BLOCK_THRESHOLD = SELECT_BLOCK_SIZE * SELECT_BLOCK_SIZE;

/*Требуемая памяти:
n - длина бинарного слова
    - Простейший подход (массив всех селектов):
        требуется не больше 32*n битов памяти
    - Реализованный подход:
        не больше 32*n/256 = n/8 битов для хранения индексов
        не больше 32*n/256 = n/8 битов для хранения superblockselect
        не больше 16*n/256 = n/16 битов для хранения short-блоков
        не больше 32*n/65536 = n/2048 битов для хранения long-блоков
        В сумме не больше: n/8 + n/8 + n/16 + n/2048 = 5n/16 + n/20148 <= 5n/16 + n/16 = 6n/16 = 3n/8 = n*0.375 битов памяти.
В частности, если какой-то бит будет равномерно распределен по строке (около половины вхождений приходится на этот бит),
то простейший подход потребует около 16*n битов памяти для хранения структуры select для этого бита, в то время как
реализованный подходи потребует около n*0.1875 битов памяти. */

struct Select {
    uint8_t* data;
    uint32_t* blocksIndices;
    uint32_t* superblockselect;
    uint16_t* shortBlocks;
    uint32_t* longBlocks;

    uint32_t blocksCount;
    uint32_t longBlocksCount;
    uint32_t shortBlocksCount;

    void Initialize(const Utility::BitField& bitStr, uint8_t bit) {
        uint64_t bitsCount;
        std::vector<bool> isLongBlock;
        std::vector<uint32_t> explicitSelect;

        isLongBlock.reserve(bitStr.Length() / SELECT_BLOCK_SIZE + 1);
        explicitSelect.reserve(bitStr.Length());

        bit &= 1ui8;
        CalculateData(bitStr, bit, bitsCount, longBlocksCount, isLongBlock, explicitSelect);
        // blocksCount <= n / log^2(n)
        // longBlocksCount <= n / log^4(n)
        blocksCount = std::max(1ui32, (uint32_t)ceil(double(bitsCount) / SELECT_BLOCK_SIZE));
        shortBlocksCount = blocksCount - longBlocksCount;

        AllocateMemory(shortBlocksCount, longBlocksCount);
        FillMemory(bitStr, bit, isLongBlock, explicitSelect);
    }

    void Deinitialize() {
        delete[] data;
    }

    // index = 0, 1, ...
    uint64_t operator[](uint64_t index) const {
        auto blockIdx = index / SELECT_BLOCK_SIZE;

        if (blockIdx >= blocksCount) {
            return 0ui32;
        }

        if (IsLongBlock(blockIdx)) {
            return longBlocks[blocksIndices[blockIdx] + index % SELECT_BLOCK_SIZE];
        }

        return superblockselect[blockIdx] + shortBlocks[blocksIndices[blockIdx] + index % SELECT_BLOCK_SIZE];
    }

    // Для отладки:
    void PrintDebugInfo() const {
        printf("blocksIndices: ");
        for (auto i = 0ui8; i < blocksCount; i++) {
            printf("0: %u%s", blocksIndices[i], i == blocksCount - 1 ? "\n" : ", ");
        }

        printf("superblockselect: ");
        for (auto i = 0ui8; i < blocksCount; i++) {
            printf("0: %u%s", superblockselect[i], i == blocksCount - 1 ? "\n" : ", ");
        }

        printf("blocks total: %u; short blocks: %u; long blocks: %u\n",
            blocksCount, shortBlocksCount, longBlocksCount);
    }

private:
    // Считает количество вхождений бита bit в строку bitStr
    void CalculateData(const Utility::BitField& bitStr, uint8_t bit,
        uint64_t& bitsCount, uint32_t& longBlocksCount, std::vector<bool>& isLongBlock, std::vector<uint32_t>& explicitSelect) const {
        auto blockStart = 0ui32, currentPosition = 0ui32;

        bitsCount = 0ui32;
        longBlocksCount = 0ui32;

        for (size_t i = 0; i < bitStr.Length(); i++) {
            if (bitStr[i] == bit) {
                bitsCount++;
                explicitSelect.push_back(uint32_t(i + 1));

                if (blockStart == 0) {
                    blockStart = uint32_t(i + 1);
                }
                currentPosition = uint32_t(i + 1);
                
                // Накопили блок
                if (bitsCount % SELECT_BLOCK_SIZE == 0) {
                    // Проверяем, является ли он long-блоком
                    if (currentPosition - blockStart + 1 > SELECT_LONG_BLOCK_THRESHOLD) {
                        longBlocksCount++;
                        isLongBlock.push_back(true);
                    } else {
                        isLongBlock.push_back(false);
                    }

                    blockStart = 0;
                    currentPosition = 0;
                }
            }
        }

        // Если оставшихся битов не накопилось на целый блок, то обрабатываем остаток аналогично
        if (blockStart != 0 && currentPosition != 0) {
            if (currentPosition - blockStart + 1 > SELECT_LONG_BLOCK_THRESHOLD) {
                longBlocksCount++;
                isLongBlock.push_back(true);
            } else {
                isLongBlock.push_back(false);
            }
        }
    }

    void FillMemory(const Utility::BitField& bitStr, uint8_t bit,
        const std::vector<bool>& isLongBlock, const std::vector<uint32_t>& explicitSelect) {
        auto shortBlockIdx = 0ui32;
        auto longBlockIdx = 0ui32;
        auto selectIdx = 0ui32;

        // Заполняем значения по блокам
        for (size_t blockIdx = 0; blockIdx < isLongBlock.size(); blockIdx++) {
            auto& idx = isLongBlock[blockIdx] ? longBlockIdx : shortBlockIdx;
            auto upperBound = std::min((uint32_t)explicitSelect.size(), selectIdx + SELECT_BLOCK_SIZE);
            blocksIndices[blockIdx] = idx;

            superblockselect[blockIdx] = explicitSelect[selectIdx];

            if (isLongBlock[blockIdx]) {
                SetBlockAsLong(uint32_t(blockIdx));

                for (; selectIdx < upperBound; selectIdx++, idx++) {
                    longBlocks[idx] = explicitSelect[selectIdx];
                }
            } else {
                for (; selectIdx < upperBound; selectIdx++, idx++) {
                    shortBlocks[idx] = uint16_t(explicitSelect[selectIdx] - superblockselect[blockIdx]);
                }
            }
        }
    }

    void AllocateMemory(uint64_t shortBlocksCount, uint64_t longBlocksCount) {
        // Все размеры в байтах
        auto blocksFlagsSize = std::max(1ui32, blocksCount / 8ui32);
        auto blocksIndicesSize = blocksCount * sizeof(uint32_t);
        auto superblockSize = blocksCount * sizeof(uint32_t);
        auto longBlocksSize = longBlocksCount * sizeof(uint32_t) * SELECT_BLOCK_SIZE;
        auto shortBlocksSize = shortBlocksCount * sizeof(uint16_t) * SELECT_BLOCK_SIZE;

        auto totalSize = blocksFlagsSize + blocksIndicesSize + superblockSize +
            longBlocksSize + shortBlocksSize;

        data = new uint8_t[totalSize];
        memset((void*)data, 0, totalSize);

        blocksIndices = (uint32_t*)&data[blocksFlagsSize];
        superblockselect = (uint32_t*)&data[blocksFlagsSize + blocksIndicesSize];
        longBlocks = (uint32_t*)&data[totalSize - longBlocksSize - shortBlocksSize];
        shortBlocks = (uint16_t*)&data[totalSize - shortBlocksSize];
    }

    bool IsLongBlock(uint64_t blockIdx) const {
        return (data[blockIdx / 8] >> (7 - (blockIdx % 8))) & 1ui8;
    }

    void SetBlockAsLong(uint64_t blockIdx) {
        data[blockIdx / 8] |= 1ui8 << (7 - (blockIdx % 8));
    }
};