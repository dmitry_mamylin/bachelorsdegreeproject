﻿#pragma once

#include <string>
#include <cinttypes>
#include <algorithm>

#include "../BitField.h"
#include "../SuffixArrayUtilities.h"

#include "Rank1Structures.h"
#include "SelectStructures.h"
#include "BWT.h"
#include "SuffixArraySamples.h"
#include "InversedSuffixArraySamples.h"

class SimpleSuccinctBinaryString {
private:
    uint64_t mRealLength; // Не учитывая дополнительных нулей в конце

    Rank1 Rank1Struct;

    Select Select1Struct;
    Select Select0Struct;

    BWT BWTStruct;

    SuffixArraySamples SuffixArrayStruct;
    InversedSuffixArraySamples InversedSuffixArrayStruct;

    void Initialize(const Utility::BitField& bits) {
        const uint64_t n = bits.Length();
        uint32_t* SA = new uint32_t[2 * (n + 1)];
        uint32_t* SAinv = SA + n + 1; // Обратный к SA

        Utility::CreateSuffixArray(bits, SA, SAinv);

        BWTStruct.Initialize(bits, SA);
        mRealLength = n; // Длина строки bits с приписанным в конце $

        // Добавляем несколько нулевых битов (не больше 15), чтобы выровнять длину строки
        auto bitsToAdd = BWTStruct.rawBWTString.Length() % RANK_BLOCK_SIZE;
        for (auto i = 0ui32; i < bitsToAdd; i++) {
            BWTStruct.rawBWTString.AddBit(0);
        }

        Rank1Struct.Initialize(BWTStruct.rawBWTString);

        Select1Struct.Initialize(BWTStruct.rawBWTString, 1ui8);
        Select0Struct.Initialize(BWTStruct.rawBWTString, 0ui8);

        SuffixArrayStruct.Initialize(SA, n + 1);
        InversedSuffixArrayStruct.Initialize(SAinv, n + 1);

        delete[] SA;
    }

    // Возвращает длину строки с добавленным в конце символов '$'
    uint64_t AugmentedLength() const {
        return mRealLength + 1;
    }

    // index = 1, 2, ..., n+1
    uint64_t Rank1(uint64_t index) const {
        return Rank1Struct[--index];
    }

    // index = 1, 2, ..., n+1
    uint64_t Rank0(uint64_t index) const {
        return index - Rank1(index);
    }

    // index = 1, 2, ..., n+1
    uint64_t Rank(uint8_t bit, uint64_t index) const {
        if (index == 0) {
            return 0;
        }

        if (index - 1 == BWTStruct.wildcardPosition) {
            return Rank(bit, index - 1);
        }

        index = index - 1 < BWTStruct.wildcardPosition ? index : index - 1;

        return bit == 0 ? Rank0(index) : Rank1(index);
    }

    // index = 1, 2, ..., n+1
    uint64_t Select1(uint64_t index) const {
        return Select1Struct[index - 1];
    }

    // index = 1, 2, ..., n+1
    uint64_t Select0(uint64_t index) const {
        return Select0Struct[index - 1];
    }

    // index = 1, 2, ..., n+1
    uint64_t Select(uint8_t bit, uint64_t index) const {
        uint64_t pos = (bit ? Select1Struct : Select0Struct)[index - 1];
        return pos - 1 >= BWTStruct.wildcardPosition ? pos + 1 : pos;
    }

    /* Вычисляет интервалы вхождений строки pat в матрицу BWM.
    Сохраняет начало в s, а конец в e. Если e < s, то вхождений нет.
    s, e = 1, 2, ..., n+1 */
    void ComputeOccInterval(const Utility::BitField& pat, uint64_t& s, uint64_t& e) const {
        s = 1;
        e = AugmentedLength();
        uint64_t C[2] = { 1, 1 + Rank(0, e) };

        for (int64_t i = pat.Length() - 1; i >= 0 && s <= e; i--) {
            uint8_t c = pat[i];

            s = C[c] + Rank(c, s - 1) + 1;
            uint64_t endRank = Rank(c, e);
            e = C[c] + endRank;
        }
    }

    void ComputeOccInterval(const std::string& pat, uint64_t& s, uint64_t& e) const {
        s = 1;
        e = AugmentedLength();
        uint64_t C[2] = { 1, 1 + Rank(0, e) };

        for (int64_t i = pat.length() - 1; i >= 0 && s <= e; i--) {
            uint8_t c = pat[i] - '0';

            s = C[c] + Rank(c, s - 1) + 1;
            uint64_t endRank = Rank(c, e);
            e = C[c] + endRank;
        }
    }

    /* LF-mapping; LF(i) = C(L[i]) + Occ(L[i], i)
    i = 0, 1, ..., n */
    uint32_t LF(uint32_t i) const {
        uint64_t C[3] = { 0, 1, 1 + Rank(0, AugmentedLength()) };
        const char c = BWTStruct[i];
        const uint8_t cIdx = c == '$' ? 0 : (1 + c - '0');
        uint64_t occ;

        if (c == '$') {
            occ = i < BWTStruct.wildcardPosition ? 0 : 1;
        }
        else {
            occ = Rank(c - '0', i + 1);
        }

        // Формула для значений в пределах [1, n+1], так что уменьшаем на 1:
        return uint32_t(C[cIdx] + occ - 1);
    }

public:
    SimpleSuccinctBinaryString(const std::string& bitStr) {
        Utility::BitField str;

        str.Allocate(bitStr.length());
        for (auto bit : bitStr) {
            str.AddBit(bit == '0' ? 0 : 1);
        }
        mRealLength = str.Length();

        Initialize(str);
    }

    SimpleSuccinctBinaryString(const SimpleSuccinctBinaryString& other) :
        mRealLength(other.mRealLength),
        BWTStruct(other.BWTStruct)
    {
        Rank1Struct.Initialize(BWTStruct.rawBWTString);

        Select1Struct.Initialize(BWTStruct.rawBWTString, uint8_t(1));
        Select0Struct.Initialize(BWTStruct.rawBWTString, uint8_t(0));
    }

    SimpleSuccinctBinaryString(const Utility::BitField& bitStr) :
        mRealLength(bitStr.Length())
    {
        Initialize(bitStr);
    }

    ~SimpleSuccinctBinaryString() {
        Rank1Struct.Deinitialize();
        Select1Struct.Deinitialize();
        Select0Struct.Deinitialize();
        SuffixArrayStruct.Deinitialize();
        InversedSuffixArrayStruct.Deinitialize();
    }

    uint64_t Length() const {
        return mRealLength;
    }

    // Считает количество вхождений строки pat в данную строку
    uint64_t OccurencesCount(const Utility::BitField& pat) const {
        uint64_t s, e;
        ComputeOccInterval(pat, s, e);
        return e < s ? 0 : e - s + 1;
    }

    // Считает количество вхождений строки pat в данную строку
    uint64_t OccurencesCount(const std::string& pat) const {
        uint64_t s, e;
        ComputeOccInterval(pat, s, e);
        return e < s ? 0 : e - s + 1;
    }

    /* Сохраняет в occs все вхождения строки pat в данную строку.
    Возвращает количество вхождений */
    uint64_t LocateOccurences(const std::string& pat, std::vector<uint32_t>& occs) const {
        uint64_t s, e;
        ComputeOccInterval(pat, s, e);

        if (e < s) {
            return 0;
        }

        e--; // Делаем значения от 0 до n
        s--;

        for (uint64_t i = s; i <= e; i++) {
            uint64_t stepsCount = 0;
            uint64_t pos = i;

            while (!SuffixArrayStruct.IsSample(pos)) {
                pos = LF(uint32_t(pos));
                stepsCount++;
            }

            occs.push_back(uint32_t(pos + stepsCount));
        }

        return e - s + 1;
    }

    /* Сохраняет в storage подстроку T[s, e] в обратном порядке.
    s, e = 1, 2, ..., n = mRealLength (длина строки без $ в конце) */
    void ReadSegment(const uint32_t s, const uint32_t e, std::string& storage) const {
        int64_t bitsToRead = int64_t(e) - int64_t(s) + 1;

        uint32_t nearestSample = InversedSuffixArrayStruct.GetNearestSample(e - 1);
        uint32_t distance = InversedSuffixArrayStruct.GetDistanceToNearestSample(e - 1);

        while (distance > 0) {
            nearestSample = LF(nearestSample);
            distance--;
        }

        while (bitsToRead > 0) {
            storage += BWTStruct[nearestSample];
            bitsToRead--;
            nearestSample = LF(nearestSample);
        }
    }
};