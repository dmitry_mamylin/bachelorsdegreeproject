﻿#pragma once

#include "../BitField.h"

constexpr uint32_t RANK_BLOCK_SIZE = 16; // В общем случае: log(n)
constexpr uint32_t RANK_SUPERBLOCK_SIZE = 256; // В общем случаем: log^2(n)
constexpr uint32_t SMALL_RANK_SIZE = 1ui32 << RANK_BLOCK_SIZE;

struct RankArray {
    uint64_t patternRanks = 0ui64;

    /* Ранг элемента в блоке может принимать 17 различных значений:
    0, 1, ..., 16, вообще говоря, требуется 5 битов на каждое значение.
    Но заметитм, что для первых 15-ти элементов достаточно 4 битов на элемент, 
    а значение 16 может принимать только последний элемент, т.е. только
    ему требуется 5 битов.
    Также заметим, что любые 2 последовательных значения ранга различаются не
    более чем на 1.
    Тогда: будем выделять на все элементы по 4 бита, а в последнем будем хранить
    либо значение явно (если оно < 16), либо что-то, что отличается от предыдущего
    более чем на 1 (например, 10).
    При такой архитектуре понадобится: 16*4 = 64 бита, что помещается в uint64_t.*/

    // i = 0, 1, ..., 15
    uint8_t operator[](uint8_t i) const {
        auto rank = uint8_t((patternRanks >> (60 - i * 4)) & 0xf);

        if (i != 15) {
            return rank;
        }

        // Предыдущее (при i=14) значение ранга
        auto prevRank = uint8_t((patternRanks >> (60 - (i - 1) * 4)) & 0xf);

        return (uint8_t)abs(rank - prevRank) > 1ui8 ? 16ui8 : rank;
    }

    // i = 0, 1, ..., 15
    void SetRank(uint8_t i, uint64_t data) {
        uint8_t shift = 4ui8 * (15 - i);
        uint64_t nullMask = 0xffffffffffffffffui64 ^ (0xfui64 << shift);
        data = (data & 0xfui64) << shift;
        patternRanks = (patternRanks & nullMask) | data;
    }
};

struct Smallrank1 {
    RankArray* data = nullptr;

    void Initialize() {
        data = new RankArray[SMALL_RANK_SIZE];

        // Идем по каждому паттерну и вычисляем его ранги
        for (auto pattern = 0ui32; pattern < SMALL_RANK_SIZE; pattern++) {
            data[pattern] = ComputeRank(pattern);
        }
    }

    void Deinitialize() {
        if (data) {
            delete[] data;
            data = nullptr;
        }
    }

    RankArray operator[](uint64_t pattern) const {
        return data[pattern];
    }

private:
    RankArray ComputeRank(uint32_t pattern) {
        RankArray ranks;
        uint16_t rank = 0;

        for (auto i = 0ui32; i < RANK_BLOCK_SIZE; i++) {
            rank = (pattern >> (RANK_BLOCK_SIZE - 1 - i)) & 1ui32 ? rank + 1 : rank;

            // 15-ый ранг заполняем вручную
            if (i == 15ui32) {
                break;
            }
            ranks.SetRank((uint8_t)i, (uint64_t)rank);
        }

        // 10 гарантированно будет отличаться больше, чем на 1
        ranks.SetRank(15ui8, uint64_t(rank < 16 ? rank : 10));

        return ranks;
    }
};

struct Boundaryrank {
    uint32_t* superblockrank = nullptr;
    uint8_t* relativeRank = nullptr;

    uint64_t superblocksCount;
    uint64_t blocksCount;

    void Initialize(const Utility::BitField& bitStr) {
        superblocksCount = uint64_t(ceil(double(bitStr.Length()) / RANK_SUPERBLOCK_SIZE));
        blocksCount = uint64_t(ceil(double(bitStr.Length()) / RANK_BLOCK_SIZE));

        superblockrank = new uint32_t[superblocksCount];
        relativeRank = new uint8_t[blocksCount];

        auto rank = 0ui32;
        for (auto i = 0ui32; i < bitStr.Length(); i++) {
            if (i % RANK_SUPERBLOCK_SIZE == 0) {
                superblockrank[i / RANK_SUPERBLOCK_SIZE] = rank;
            }

            if (i % RANK_BLOCK_SIZE == 0) {
                relativeRank[i / RANK_BLOCK_SIZE] =
                    uint8_t(rank - superblockrank[i / RANK_SUPERBLOCK_SIZE]);
            }

            rank = bitStr[i] == 0ui8 ? rank : rank + 1;
        }
    }

    void Deinitialize() {
        if (superblockrank) {
            delete[] superblockrank;
            superblockrank = nullptr;
        }

        if (relativeRank) {
            delete[] relativeRank;
            relativeRank = nullptr;
        }
    }

    uint32_t operator[](uint64_t index) const {
        return superblockrank[index / RANK_SUPERBLOCK_SIZE] +
            relativeRank[index / RANK_BLOCK_SIZE];
    }
};

struct Rank1 {
    Smallrank1 smallrank;
    Boundaryrank boundaryrank;
    const Utility::BitField* string;

    void Initialize(const Utility::BitField& bitStr) {
        string = &bitStr;
        smallrank.Initialize();
        boundaryrank.Initialize(bitStr);
    }

    void Deinitialize() {
        smallrank.Deinitialize();
        boundaryrank.Deinitialize();
    }

    // index = 0, 1, ...
    uint64_t operator[](uint64_t index) const {
        auto block = ((uint16_t*)string->Data())[index / RANK_BLOCK_SIZE];
        block = (block << 8) | (block >> 8);
        return boundaryrank[index] + smallrank[block][index % RANK_BLOCK_SIZE];
    }
};