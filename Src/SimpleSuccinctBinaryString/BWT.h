﻿#pragma once

#include "../BitField.h"

#include <cinttypes>

struct BWT {
private:
    void WriteBWT(const uint32_t* sa, const Utility::BitField& str, const uint64_t n) {
        for (uint64_t i = 0; i < n + 1; i++) {
            uint32_t suffix = sa[i];

            /* Значения BWT вычисляются по формуле:
                      ( str[sa[i] - 1]; если sa[i] > 0
            BWT[i] = {
                      ( $             ; если sa[i] = 0 */
            if (suffix > 0) {
                rawBWTString.AddBit(str[suffix - 1]);
            } else {
                wildcardPosition = i;
            }
        }
    }

public:
    uint64_t wildcardPosition;
    Utility::BitField rawBWTString;

    BWT() : wildcardPosition(0) {}

    BWT(const BWT& other) :
        wildcardPosition(other.wildcardPosition),
        rawBWTString(other.rawBWTString) {}

    /* str - бинарная строка длины n
    n+1 - длина строки str с добавленным в конце $
    sa - массив длины n+1 - суффиксный массив строки str$ (с приписанным $ в конце) */
    void Initialize(const Utility::BitField& str, const uint32_t* sa) {
        const uint64_t n = str.Length();

        rawBWTString.Allocate(n);

        WriteBWT(sa, str, n);
    }

    // index = 0, 1, ..., n
    char operator[](uint64_t index) const {
        if (index < wildcardPosition) {
            return '0' + rawBWTString[index];
        }

        if (index > wildcardPosition) {
            return '0' + rawBWTString[index - 1];
        }

        return '$';
    }
};