#pragma once

#include <cstdint>

/* ��������� ���������� ������ SA ������ � ����� SAMPLE_DISTANCE, � ����� �������
(x = 0, PART_SIZE, 2*PART_SIZE, ... - ���� �����) ������ �������� SA[x] */
struct SuffixArraySamples {
    static constexpr uint32_t SAMPLE_DISTANCE = 256;

    uint32_t* samples = nullptr;
    uint32_t samplesCount;

    /* SA - ���������� ������ ����� N */
    void Initialize(const uint32_t* SA, const uint64_t N) {
        samplesCount = uint32_t(ceil(double(N) / SAMPLE_DISTANCE));
        samples = new uint32_t[samplesCount];

        for (uint64_t i = 0; i < N; i += SAMPLE_DISTANCE) {
            samples[i / SAMPLE_DISTANCE] = SA[i];
        }
    }

    void Deinitialize() {
        if (samples) {
            delete[] samples;
        }
    }

    bool IsSample(uint64_t x) const {
        return x % SAMPLE_DISTANCE == 0;
    }
};