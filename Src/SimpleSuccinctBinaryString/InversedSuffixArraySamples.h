#pragma once

#include <cinttypes>

/* ��������� �������� ������ � SuffixArray ����� N ������ � ����� h = SAMPLE_DISTANCE.
� i-�� ���� ������ �������� i+1-�� ����.
������� ����� ����� �����: h, 2h, ..., (k-1)h, min(kh, N-1).
�.�. ������� �������� ������� �������� �� � 0-�� �������, � � h-��.
N = n+1, ��� n - ����� �������� ������, � n+1 - ����� ������, � ����������� � �����
�������� $, ��� ������� Suffix Array � ��� ��������. */
struct InversedSuffixArraySamples {
    static constexpr uint32_t SAMPLE_DISTANCE = 256;

    uint32_t* samples = nullptr;
    uint32_t samplesCount;
    uint32_t lastSample;
    
    void Initialize(const uint32_t* SAinv, const uint64_t N) {
        lastSample = uint32_t(N - 1);
        samplesCount = uint32_t((N - 1) / SAMPLE_DISTANCE);

        if (lastSample - samplesCount * SAMPLE_DISTANCE > 1) {
            samplesCount++;
        }

        samples = new uint32_t[samplesCount];

        uint64_t nextIdx = 0;
        uint64_t i = 0;
        while (i < samplesCount) {
            nextIdx += SAMPLE_DISTANCE;
            nextIdx = nextIdx >= N - 1 ? N - 2 : nextIdx;

            samples[i] = SAinv[nextIdx + 1];
            i++;
        }
    }

    void Deinitialize() {
        if (samples) {
            delete[] samples;
        }
    }

    /* ���������� �������� ���� �����, ���������� ����� � ��������� x.
    x = 0, 1, ..., N - 1 */
    uint32_t GetNearestSample(uint32_t i) const {
        uint32_t nearestIdx = i / SAMPLE_DISTANCE;
        if (i != 0 && i % SAMPLE_DISTANCE == 0) {
            nearestIdx--;
        }

        return samples[nearestIdx];
    }

    /* ���������� ���������� �� ���������� � i ����� ���� �����.
    i = 0, 1, ..., N-1 */
    uint32_t GetDistanceToNearestSample(uint32_t i) const {
        // ��� ��������, ��� ������ � ��������� ����, ��� �������� ��������� �����
        if (i / SAMPLE_DISTANCE == samplesCount - 1 && i % SAMPLE_DISTANCE != 0) {
            return lastSample - i - 1;
        } 

        if (i % SAMPLE_DISTANCE == 0 && i != 0) {
            return 0;
        }

        if (i == 0 && lastSample <= SAMPLE_DISTANCE) {
            return lastSample - 1;
        }

        return (SAMPLE_DISTANCE - (i % SAMPLE_DISTANCE));
    }
};