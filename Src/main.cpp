﻿#include "SimpleSuccinctBinaryString/SimpleSuccinctBinaryString.h"
#include "RRRSuccinctBinaryString/RRRSuccinctBinaryString.h"

#include "Tests/SimpleSuccinctBinaryStringTests/OccurencesCount.h"
#include "Tests/SimpleSuccinctBinaryStringTests/OccurencesLocations.h"
#include "Tests/SimpleSuccinctBinaryStringTests/ReadSegment.h"

constexpr uint64_t STRING_LENGTH = uint64_t(1) << 33; // 1024 MB
//constexpr uint64_t STRING_LENGTH = 6710886400; // 800 MB
constexpr uint64_t TESTS_COUNT = 1;

bool DoMatchingTest(const Utility::BitField& bits) {
    puts("Creating RRR...");
    RRR::RRRSuccinctBinaryString rrr(bits);
    puts("Done\n");

    puts("Storage info:");
    printf("Raw string uses %.3lf MB\n", bits.Length() / double(8 * 1048576));
    printf("RRR string uses %.3lf MB\n", rrr.PackedStringSizeMB());
    printf("Decode table uses: %.3lf MB\n", rrr.DecodeTableSizeMB());
    printf("Rank structure uses %.3lf MB\n", rrr.RankStructureSizeMB());

    puts("\nComputing...");
    uint64_t expectedRank1 = 0;
    for (uint64_t i = 0; i < bits.Length(); i++) {
        expectedRank1 += bits[i];

        if (bits[i] != rrr[i]) {
            printf("String length: %llu\n", bits.Length());
            printf("Mismatch in: %llu\n", i);
            printf("String's output: %hhu\n", bits[i]);
            printf("RRR's output: %hhu\n", rrr[i]);

            return false;
        }

        uint64_t givenRank1 = rrr.Rank(1, i);
        if (expectedRank1 != givenRank1) {
            printf("Rank1 mismatch in %llu: Given=%llu; expected=%llu\n",
                i, givenRank1, expectedRank1);

            return false;
        }
    }

    return true;
}

void GenerateGoodString(Utility::BitField& bits, const uint64_t length) {
    std::default_random_engine engine((unsigned)time(NULL));
    std::uniform_int_distribution<int> distr(0, 1);

    uint8_t blocks[2] = { 0x00, 0xff };

    bits.Allocate(length);
    for (uint64_t i = 0; 16*i < length; i++) {
        uint8_t byte = blocks[distr(engine)];
        bits.AddByteUnsafe(byte);
        bits.AddByteUnsafe(byte);
    }
}

void GenerateBadString(Utility::BitField& bits, const uint64_t length) {
    std::default_random_engine engine((unsigned)time(NULL));
    std::uniform_int_distribution<int> distr(0, 1);

    uint8_t blocks[2] = { 0x00, 0xff };

    bits.Allocate(length);
    for (uint64_t i = 0; 16 * i < length; i++) {
        uint8_t byteIdx = distr(engine);
        bits.AddByteUnsafe(blocks[byteIdx]);
        bits.AddByteUnsafe(blocks[(byteIdx + 1) % 2]);
    }
}

void CreateRRR(const Utility::BitField& bits) {
    puts("Creating RRR...");
    RRR::RRRSuccinctBinaryString rrr(bits);
    puts("Done\n");

    puts("Storage info:");
    printf("Raw string uses %.3lf MB\n", bits.Length() / double(8 * 1048576));
    printf("RRR string uses %.3lf MB\n", rrr.PackedStringSizeMB());
    printf("Decode table uses: %.3lf MB\n", rrr.DecodeTableSizeMB());
    printf("Rank structure uses %.3lf MB\n", rrr.RankStructureSizeMB());
}

int main() {
    /*bool success = true;

    printf("Performing test for binary string with size %.3lf MB...\n\n",
        STRING_LENGTH / double(8 * 1048576));
    for (uint64_t testIdx = 0; testIdx < TESTS_COUNT; testIdx++) {
        BitField bits;
        puts("Generating random binary string...");
        SimpleSuccinctBinaryStringTests::RandomizeBinaryString(bits, STRING_LENGTH);
        puts("Done\n");

        if (!(success = DoTest(bits))) {
            printf("Error in test #%llu\n", testIdx);
            break;
        }
    }

    if (success) {
        puts("Done!");
    } else {
        puts("Stopped!");
    }*/

    puts("Generating bit string...");
    Utility::BitField bits;
    //GenerateBadString(bits, STRING_LENGTH);
    GenerateGoodString(bits, STRING_LENGTH);
    puts("Done");
    CreateRRR(bits);

    system("pause");
}