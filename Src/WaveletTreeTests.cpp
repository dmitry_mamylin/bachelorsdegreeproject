#include "WaveletTree/WaveletTree.h"

#include "Tests/SimpleSuccinctBinaryStringTests/ReadSegment.h"
#include "StringUtilities.h"

bool DoMatchingTest(const std::string& str) {
    WaveletTree::WaveletTree wtree(str);

    for (uint64_t i = 0; i < wtree.Length(); i++) {
        if (wtree[i] != str[i]) {
            return false;
        }
    }

    return true;
}

void DoStorageTest(const uint64_t rawStringLength) {
    std::string str;
    str.reserve(rawStringLength);

    Utility::RandomizeString(str, rawStringLength, 'a', 'z');
    
    WaveletTree::WaveletTree wtree(str);

    printf("Wavelet tree's RRR packed string size: %lf MB\n", wtree.RRRPackedStringSizeMB());
    printf("Wavelet tree's rank structure size: %lf MB\n", wtree.RRRRankStructureSizeMB());
    printf("Wavelet tree's RRR total size: %lf MB\n", wtree.RRRSizeMB());
    printf("Wavelet tree's nodes storage size: %lf MB\n", wtree.NodesStorageSizeMB());
    printf("Wavelet tree consumes (total size): %lf MB\n", wtree.RRRSizeMB() + wtree.NodesStorageSizeMB());
    printf("Raw string consumes: %lf MB\n", str.length() / 1048576.0);
}

#include <string>
#include <fstream>
#include <streambuf>
void DoFileStorageTest(const std::string& filename) {
    std::ifstream t(filename);
    std::string str;

    if (!t.is_open()) {
        printf("Error! Cannot open file: ");
        puts(filename.c_str());
        return;
    }

    t.seekg(0, std::ios::end);
    str.reserve(t.tellg());
    t.seekg(0, std::ios::beg);

    str.assign((std::istreambuf_iterator<char>(t)),
        std::istreambuf_iterator<char>());

    WaveletTree::WaveletTree wtree(str);

    printf("Wavelet tree's RRR packed string size: %lf MB\n", wtree.RRRPackedStringSizeMB());
    printf("Wavelet tree's rank structure size: %lf MB\n", wtree.RRRRankStructureSizeMB());
    printf("Wavelet tree's RRR total size: %lf MB\n", wtree.RRRSizeMB());
    printf("Wavelet tree's nodes storage size: %lf MB\n", wtree.NodesStorageSizeMB());
    printf("Wavelet tree consumes (total size): %lf MB\n", wtree.RRRSizeMB() + wtree.NodesStorageSizeMB());
    printf("Raw file consumes: %lf MB\n", str.length() / 1048576.0);
}

bool DoFileMatchingTest(const std::string& filename) {
    std::ifstream t(filename);
    std::string str;

    if (!t.is_open()) {
        printf("Cannot open file: ");
        puts(filename.c_str());
        return false;
    }

    t.seekg(0, std::ios::end);
    str.reserve(t.tellg());
    t.seekg(0, std::ios::beg);

    str.assign((std::istreambuf_iterator<char>(t)),
        std::istreambuf_iterator<char>());

    WaveletTree::WaveletTree wtree(str);

    printf("Wavelet tree's RRR packed string size: %lf MB\n", wtree.RRRPackedStringSizeMB());
    printf("Wavelet tree's rank structure size: %lf MB\n", wtree.RRRRankStructureSizeMB());
    printf("Wavelet tree's RRR total size: %lf MB\n", wtree.RRRSizeMB());
    printf("Wavelet tree's nodes storage size: %lf MB\n", wtree.NodesStorageSizeMB());
    printf("Wavelet tree consumes (total size): %lf MB\n", wtree.RRRSizeMB() + wtree.NodesStorageSizeMB());
    printf("Raw file consumes: %lf MB\n", str.length() / 1048576.0);

    for (uint64_t i = 0; i < str.length(); i++) {
        if (str[i] != wtree[i]) {
            printf("Mismatch in position: %llu\n", i);
            return false;
        }
    }

    return true;
}

#include <set>
bool DoRankTest(const uint64_t length, uint8_t firstLetter, uint8_t lastLetter) {
    std::string str;
    std::set<char> alphabet;

    str.reserve(length);

    Utility::RandomizeString(str, length, firstLetter, lastLetter);

    for (char c : str) {
        alphabet.insert(c);
    }

    WaveletTree::WaveletTree wtree(str);

    for (char c : alphabet) {
        uint64_t expectedRank = 0;
        for (uint64_t i = 0; i < str.length(); i++) {
            expectedRank += str[i] == c ? 1 : 0;
            uint64_t givenRank = wtree.Rank(c, i);

            if (expectedRank != givenRank) {
                return false;
            }
        }
    }

    return true;
}

constexpr uint64_t LEN = 1 << 20;
constexpr uint64_t TESTS_COUNT = 10;

int main(int argc, char** argv) {
    for (uint64_t i = 0; i < TESTS_COUNT; i++) {
        std::string str;
        str.reserve(LEN);

        Utility::RandomizeString(str, LEN, 1, 255);
        
        if (!DoMatchingTest(str)) {
            puts("Error!");
            break;
        }
        
        if (i == TESTS_COUNT-1) {
            puts("Success!");
        }
    }

    /*DoStorageTest(LEN);
    DoFileStorageTest("dna");*/

    /*if (DoFileMatchingTest("dna")) {
        puts("Success!");
    } else {
        puts("Error!");
    }*/

    /*if (DoFileMatchingTest(argv[1])) {
        puts("Success!");
    } else {
        puts("Error!");
    }*/

    for (uint64_t i = 0; i < TESTS_COUNT; i++) {
        if (!DoRankTest(LEN, 1, 255)) {
            puts("Error!");
            break;
        }

        if (i == TESTS_COUNT - 1) {
            puts("Success!");
        }
    }
    
    system("pause");

    return 0;
}