#pragma once

#include <string.h>
#include <cstdint>
#include <stdlib.h>
#include <time.h>

#include "../../SimpleSuccinctBinaryString/SimpleSuccinctBinaryString.h"

namespace SimpleSuccinctBinaryStringTests {
    /* ��������� ������� ���������� ��������� ������ P � ������ T.
    ��������� ������� ������ T � 1 */
    uint64_t OccCount(const char* T, const char* P) {
        const char* firstOcc = strstr(T, P);
        uint64_t occ = 0;

        while (firstOcc) {
            occ++;
            T = firstOcc + 1;

            firstOcc = strstr(T, P);
        }

        return occ;
    }

    /* ��� ������ ������ T ���������� ��� �� ��������� � ������� ��� ������ ���������
    ���������� ��������� � T ������� ���������, � ����� �������� � ������� BWT � ����������
    ���������� ������ */
    bool TestOccCount(const char* T) {
        const uint64_t n = strlen(T);
        char* P = new char[n + 1];

        SimpleSuccinctBinaryString str(T);

        for (uint64_t i = 1; i <= n; i++) {
            for (uint64_t j = 0; j < n; j += i) {
                strcpy(P, T + j);
                P[n] = '\0';

                uint64_t expected = OccCount(T, P);
                uint64_t given = str.OccurencesCount(P);

                if (given != expected) {
                    delete[] P;
                    return false;
                }
            }
        }

        delete[] P;

        return true;
    }

    /* ��� ������ ������ T testsCount ��� ������� ��������� ������ � ������� �� ����������
    ��������� � T ������� ���������, � ����� � ������� BWT. ���������� ���������� ���������� */
    bool TestRandomOccCount(const char* T, uint64_t testsCount) {
        const uint64_t n = strlen(T);
        const uint64_t maxRandLen = 2 * n;
        char* P = new char[maxRandLen + 1];

        SimpleSuccinctBinaryString str(T);

        srand((unsigned int)time(NULL));

        for (uint64_t testIdx = 0; testIdx < testsCount; testIdx++) {
            uint64_t m = 1 + rand() % maxRandLen;

            for (uint64_t i = 0; i < m; i++) {
                P[i] = (rand() % 2) ? '1' : '0';
            }
            P[m] = '\0';

            uint64_t expected = OccCount(T, P);
            uint64_t given = str.OccurencesCount(P);

            if (expected != given) {
                return false;
            }
        }

        delete[] P;

        return true;
    }

    void DoOccurencesCountTests(const char* T) {
        printf("Testing all substrings occurences... %s\n",
            TestOccCount(T) ? "Done!" : "Error!");
        printf("Testing random substrings occurences... %s\n",
            TestRandomOccCount(T, 1000000) ? "Done!" : "Error!");
    }
}