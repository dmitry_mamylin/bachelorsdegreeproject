﻿#pragma once

#include <cstdint>
#include <cstdio>
#include <cstring>
#include <string>
#include <random>
#include <ctime>

#include "../../SimpleSuccinctBinaryString/SimpleSuccinctBinaryString.h"

namespace SimpleSuccinctBinaryStringTests {
    bool TestReadSegment(const char* T) {
        const uint64_t n = strlen(T);
        SimpleSuccinctBinaryString str(T);
        std::string storage;

        storage.reserve(n);

        for (uint32_t s = 1; s <= n; s++) {
            for (uint32_t e = s; e <= n; e++) {
                storage.clear();

                str.ReadSegment(s, e, storage);

                if (storage.size() != e - s + 1) {
                    puts("Wrong segment length.");
                    return false;
                }

                uint64_t i = s - 1;
                for (auto it = storage.rbegin(); it != storage.rend(); ++it) {
                    if (*it != T[i]) {
                        puts("Substring mismatch:");
                        reverse(storage.begin(), storage.end());
                        printf("%s <- given\n", storage.c_str());

                        for (int64_t k = s - 1; k < e; k++) {
                            putchar(T[k]);
                        }
                        printf(" <- expected\n");

                        printf("In range: s = %u, e = %u.\n", s, e);
                        printf("Raw lenght of string: %llu; Augmented length of string: %llu.\n", n, n + 1);

                        return false;
                    }

                    i++;
                }
            }
        }
        return true;
    }

    // Принимает на вход массив T длины len+1 и рандомно заполняет его значениями '0' и '1'
    void RandomizeBinaryString(char* T, const uint64_t len) {
        std::default_random_engine engine((unsigned)time(NULL));
        std::uniform_int_distribution<int> distr('0', '1');

        for (uint64_t i = 0; i < len; i++) {
            T[i] = char(distr(engine));
        }
        T[len] = '\0';
    }

    void RandomizeBinaryString(std::string& T, const uint64_t len) {
        std::default_random_engine engine((unsigned)time(NULL));
        std::uniform_int_distribution<int> distr('0', '1');

        T.reserve(len);

        for (uint64_t i = 0; i < len; i++) {
            T += char(distr(engine));
        }
    }

    void RandomizeBinaryString(Utility::BitField& T, const uint64_t len) {
        std::default_random_engine engine((unsigned)time(NULL));
        std::uniform_int_distribution<int> distr(0, 255);

        T.Allocate(len);

        for (uint64_t byte = 0; 8*byte < len; byte++) {
            T.AddByteUnsafe(uint8_t(distr(engine)));
        }
    }

    // Тестирует считывание всех подстрок для данной строки T
    bool DoReadSegmentTests(const char* T) {
        bool result = TestReadSegment(T);
        printf("Testing all substrings reading... %s\n",
            result ? "Done!" : "Error!");
        return result;
    }

    // Тестирует считываение всевозможных подстрок для рандомных строк с длиной в диапазоне [len1, len2]
    bool DoRangeLengthsTest(const uint32_t len1, const uint32_t len2) {
        char* T = new char[len2 + 1];

        uint32_t startLen = len1;
        while (startLen <= len2) {
            RandomizeBinaryString(T, startLen);
            printf("Length = %u: ", startLen);

            if (!DoReadSegmentTests(T)) {
                delete[] T;
                return false;
            }

            startLen++;
        }

        delete[] T;

        return true;
    }
}