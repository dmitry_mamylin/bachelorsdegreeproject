#pragma once

#include <vector>
#include <cstdint>
#include <algorithm>

#include "../../SimpleSuccinctBinaryString/SimpleSuccinctBinaryString.h"

namespace SimpleSuccinctBinaryStringTests {
    /* ��������� ������� ��� ��������� ������ P � ������ T � ��������� �� � occs.
    ��������� ������� ������ T � 1 */
    uint64_t OccLocations(const char* T, const char* P, std::vector<uint32_t>& occs) {
        const char* start = T;
        const char* firstOcc = strstr(T, P);
        uint64_t occ = 0;

        while (firstOcc) {
            occs.push_back(uint32_t(firstOcc - start + 1));

            occ++;
            T = firstOcc + 1;

            firstOcc = strstr(T, P);
        }

        return occ;
    }

    /* ��� ������ ������ T ���������� ��� �� ��������� � ������� ��� ������ ���������
    ��� ��������� � T ������� ���������, � ����� �������� � ������� BWT � ����������
    ���������� ��������� ������� */
    bool TestOccLocations(const char* T) {
        const uint64_t n = strlen(T);
        char* P = new char[n + 1];

        SimpleSuccinctBinaryString str(T);

        for (uint64_t i = 1; i <= n; i++) {
            for (uint64_t j = 0; j < n; j += i) {
                strcpy(P, T + j);
                P[n] = '\0';

                std::vector<uint32_t> expected, given;

                uint64_t expectedCount = OccLocations(T, P, expected);
                uint64_t givenCount = str.LocateOccurences(P, given);

                if (givenCount != expectedCount) {
                    delete[] P;
                    return false;
                }

                for (size_t k = 0; k < given.size(); k++) {
                    uint32_t val = given[k];

                    if (!binary_search(expected.begin(), expected.end(), val)) {
                        printf("Expected locations: ");
                        for (uint64_t m = 0; m < expected.size(); m++) {
                            printf("%u%s", expected[m], m == expected.size() - 1 ? "\n" : ", ");
                        }

                        printf("Given locations: ");
                        for (uint64_t m = 0; m < given.size(); m++) {
                            printf("%u%s", given[m], m == given.size() - 1 ? "\n" : ", ");
                        }

                        delete[] P;
                        return false;
                    }
                }
            }
        }

        delete[] P;

        return true;
    }

    void DoOccurencesLocationTests(const char* T) {
        printf("Testing locations of all substrings occurences... %s\n",
            TestOccLocations(T) ? "Done!" : "Error!");
    }
}