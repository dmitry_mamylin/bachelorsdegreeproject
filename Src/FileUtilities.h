﻿#pragma once

#include <string>
#include <fstream>
#include <streambuf>

namespace Utility {
    std::string ReadFile(const std::string& filename) {
        std::ifstream t(filename);
        std::string str;

        if (!t.is_open()) {
            return str;
        }

        t.seekg(0, std::ios::end);
        str.reserve(t.tellg());
        t.seekg(0, std::ios::beg);

        str.assign((std::istreambuf_iterator<char>(t)),
            std::istreambuf_iterator<char>());

        t.close();

        return str;
    }
}