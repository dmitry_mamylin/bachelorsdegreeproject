﻿#pragma once

#include "../RRRSuccinctBinaryString/RRRSuccinctBinaryString.h"
#include "../BitField.h"

#include <cstdint>
#include <cstring>
#include <cmath>
#include <algorithm>

namespace WaveletTree {
    class WaveletTree {
    public:
        template<class STR_CLASS>
        WaveletTree(const STR_CLASS& str) {
            alphabetSize = DetermineAlphabet(str, alphabet);
            length = str.length();

            const uint64_t treeHeight = uint64_t(ceil(log2(alphabetSize))) + uint64_t(1);

            nodesCount = uint64_t(1) << treeHeight; // 2^{высота дерева}
            nodesCount--; // Количество вершин в полном дереве той же высоты (верхняя оценка для этого дерева)
            nodePosition = new uint64_t[3 * nodesCount];
            nodeRank[0] = &nodePosition[nodesCount];
            nodeRank[1] = &nodePosition[2 * nodesCount];
            realNodesCount = nodesCount;
            Utility::BitField bits;

            memset(nodePosition, 0, sizeof(uint64_t) * nodesCount * 3);

            bits.Allocate(treeHeight * str.length());
            nodesCount = CreateBitVector(bits, str); // Убираем лишние вершины

            rrrString = new RRR::RRRSuccinctBinaryString(bits);

            ComputeNodesRanks();
        }

        ~WaveletTree() {
            delete[] nodePosition;
            delete rrrString;
        }

        uint64_t Length() const {
            return length;
        }

        double RRRPackedStringSizeMB() const {
            return rrrString->PackedStringSizeMB();
        }

        double RRRRankStructureSizeMB() const {
            return rrrString->RankStructureSizeMB();
        }

        double RRRSizeMB() const {
            return rrrString->PackedStringSizeMB() + rrrString->RankStructureSizeMB();
        }

        double NodesStorageSizeMB() const {
            return 3 * realNodesCount * sizeof(uint64_t) / 1048576.0;
        }

        double TotalSizeMB() const {
            return RRRSizeMB() +
                NodesStorageSizeMB();
        }

        uint64_t Rank(const char symbol, const uint64_t i) const {
            uint64_t l = 0, r = alphabetSize;
            uint64_t node = 0;
            uint64_t rank = i + 1, pos = 0;
            const uint8_t sym = *reinterpret_cast<const uint8_t*>(&symbol);

            do {
                pos = nodePosition[node] + rank - 1;

                uint64_t lrSum = l + r;
                uint64_t center = lrSum % 2 ? 1 + lrSum / 2 : lrSum / 2;
                uint8_t bit = sym < alphabet[center] ? 0 : 1;

                //uint64_t nodeRank = rrrString->Rank(bit, nodePos);
                rank = rrrString->Rank(bit, pos) - nodeRank[bit][node] + 1;

                if (bit) {
                    l = center;
                    node = 2 * node + 2;
                } else {
                    r = center;
                    node = 2 * node + 1;
                }
            } while (r - l > 1);

            return rank;
        }

        // i = 0, 1, ..., length-1
        char operator[](const uint64_t i) const {
            uint64_t l = 0;
            uint64_t r = alphabetSize;
            uint64_t node = 0;
            uint64_t rank = i;

            do {
                uint64_t lrSum = l + r;
                uint64_t center = lrSum % 2 ? 1 + lrSum / 2 : lrSum / 2;

                uint64_t pos = nodePosition[node] + rank;
                uint8_t bit = (*rrrString)[pos];

                //uint64_t nodeRank = rrrString->Rank(bit, nodePosition[node]);
                rank = rrrString->Rank(bit, pos) - nodeRank[bit][node];

                if (bit) {
                    l = center;
                    node = 2 * node + 2;
                } else {
                    r = center;
                    node = 2 * node + 1;
                }

                if (r - l <= 1) {
                    rank = 0;
                }
            } while (l < r && node < nodesCount);

            return *reinterpret_cast<const char*>(&alphabet[l]);
        }

        // Возвращает ранк символа, стоящего на i-ом месте и сам i-ый символ
        void GetRankAndSymbol(const uint64_t i, uint64_t& outRank, uint8_t& outSymbol) const {
            uint64_t l = 0, r = alphabetSize;
            uint64_t node = 0;
            uint64_t rank = i + 1, pos = 0;

            do {
                pos = nodePosition[node] + rank - 1;

                uint64_t lrSum = l + r;
                uint64_t center = lrSum % 2 ? 1 + lrSum / 2 : lrSum / 2;
                uint8_t bit = (*rrrString)[pos];

                //uint64_t nodeRank = rrrString->Rank(bit, nodePos);
                rank = rrrString->Rank(bit, pos) - nodeRank[bit][node] + 1;

                if (bit) {
                    l = center;
                    node = 2 * node + 2;
                } else {
                    r = center;
                    node = 2 * node + 1;
                }
            } while (r - l > 1);

            outRank = rank;
            outSymbol = alphabet[l];
        }

    private:
        uint64_t* nodePosition;
        uint64_t* nodeRank[2];
        RRR::RRRSuccinctBinaryString* rrrString;

        uint64_t nodesCount;
        uint64_t realNodesCount;
        uint64_t alphabetSize;
        uint64_t length;

        uint8_t alphabet[256];

        /* Определяет алфавит и его мощность N по заданной строке str.
        alphabet - массив длины 256, первые N элементов этого массива - символы алфавита.
        Возвращает N - мощность алфавита */
        template<class STR_CLASS>
        static uint64_t DetermineAlphabet(const STR_CLASS& str, uint8_t* alphabet) {
            bool exists[256];

            memset(exists, 0, 256);

            for (char c : str) {
                uint8_t index = *reinterpret_cast<uint8_t*>(&c);
                exists[index] = true;
            }

            uint64_t charsWritten = 0;
            for (uint64_t i = 0; i < 256; i++) {
                if (exists[i]) {
                    //alphabet[charsWritten] = char(i);
                    alphabet[charsWritten] = uint8_t(i);
                    charsWritten++;
                }
            }

            return charsWritten;
        }

        struct Buffer {
            uint64_t nodesCount;
            uint64_t* nodesPositions;
            uint64_t* nodesLengths;
            char* string;
            bool* isLeaf;
        };

        // Возвращает количество вершин в дереве
        template<class STR_CLASS>
        uint64_t CreateBitVector(Utility::BitField& bits, const STR_CLASS& str) const {
            char* stringBuffers[2];
            bool leafBuffers[2][512];
            uint64_t nodesPositionsBuffers[1024];
            uint64_t nodesLengthsBuffers[1024];
            Buffer buffers[2];

            stringBuffers[0] = new char[str.length() + 1];
            stringBuffers[1] = new char[str.length() + 1];

            memset(&leafBuffers[0], false, sizeof(leafBuffers[0]));
            memset(&leafBuffers[1], false, sizeof(leafBuffers[1]));

            //strcpy(stringBuffers[0], str.c_str());
            for (size_t i = 0; i < str.length(); i++) {
                stringBuffers[0][i] = str[i];
            }

            nodesPositionsBuffers[0] = 0;
            nodesLengthsBuffers[0] = str.length();

            buffers[0].nodesCount = 1;
            buffers[0].nodesPositions = nodesPositionsBuffers;
            buffers[0].nodesLengths = nodesLengthsBuffers;
            buffers[0].string = stringBuffers[0];
            buffers[0].isLeaf = leafBuffers[0];

            buffers[1].nodesPositions = &nodesPositionsBuffers[512];
            buffers[1].nodesLengths = &nodesLengthsBuffers[512];
            buffers[1].string = stringBuffers[1];
            buffers[1].isLeaf = leafBuffers[1];

            Buffer* readBuffer = &buffers[0];
            Buffer* writeBuffer = &buffers[1];

            uint64_t nodePosIdx = 0;
            const uint64_t nonRootLevelsCount = (uint64_t)ceil(log2(alphabetSize));
            for (uint64_t level = 0; level <= nonRootLevelsCount; level++) {
                writeBuffer->nodesCount = uint64_t(1) << (level + 1);
                uint64_t nodeWritingOffset = 0;

                for (uint64_t nodeIdx = 0; nodeIdx < readBuffer->nodesCount; nodeIdx++) {
                    if (readBuffer->isLeaf[nodeIdx]) {
                        continue;
                    }

                    uint64_t alphabetSize = 0;
                    uint64_t symbolOccurencesCount[256];
                    char alphabet[256];

                    memset(symbolOccurencesCount, 0, sizeof(symbolOccurencesCount));

                    const uint64_t upperBound = readBuffer->nodesPositions[nodeIdx] + readBuffer->nodesLengths[nodeIdx];
                    for (uint64_t symIdx = readBuffer->nodesPositions[nodeIdx]; symIdx < upperBound; symIdx++) {
                        char c = readBuffer->string[symIdx];
                        uint8_t index = *reinterpret_cast<uint8_t*>(&c);
                        symbolOccurencesCount[index] += 1;
                    }

                    uint64_t alphabetIdx = 0;
                    for (uint64_t i = 0; i < 256; i++) {
                        if (symbolOccurencesCount[i]) {
                            alphabetSize++;
                            alphabet[alphabetIdx] = char(i);
                            alphabetIdx++;
                        }
                    }

                    uint64_t alphabetHalf = alphabetSize % 2 ? 1 + alphabetSize / 2 : alphabetSize / 2;
                    uint64_t bitOccurences[2] = { 0, 0 };
                    uint8_t symbolBit[256];
                    for (uint64_t i = 0; i < alphabetSize; i++) {
                        char symbol = alphabet[i];
                        uint8_t index = *reinterpret_cast<uint8_t*>(&symbol);

                        uint8_t bit = i < alphabetHalf ? 0 : 1;

                        symbolBit[index] = bit;
                        bitOccurences[bit] += symbolOccurencesCount[index];
                    }

                    if (alphabetSize > 1) {
                        writeBuffer->nodesLengths[2 * nodeIdx] = bitOccurences[0];
                        writeBuffer->nodesPositions[2 * nodeIdx] = nodeWritingOffset;
                        writeBuffer->isLeaf[2 * nodeIdx] = false;

                        writeBuffer->nodesLengths[2 * nodeIdx + 1] = bitOccurences[1];
                        writeBuffer->nodesPositions[2 * nodeIdx + 1] = nodeWritingOffset + bitOccurences[0];
                        writeBuffer->isLeaf[2 * nodeIdx + 1] = false;
                    }
                    else {
                        writeBuffer->nodesLengths[2 * nodeIdx] = bitOccurences[0];
                        writeBuffer->nodesPositions[2 * nodeIdx] = nodeWritingOffset;
                        writeBuffer->isLeaf[2 * nodeIdx] = true;
                        nodeWritingOffset++;

                        writeBuffer->nodesLengths[2 * nodeIdx + 1] = 1;
                        writeBuffer->nodesPositions[2 * nodeIdx + 1] = nodeWritingOffset;
                        writeBuffer->isLeaf[2 * nodeIdx + 1] = true;
                        nodeWritingOffset++;

                        writeBuffer->nodesCount -= 2;

                        nodePosition[nodePosIdx] = bits.Length();

                        for (uint64_t i = 0; i < bitOccurences[0]; i++) {
                            bits.AddBit(0);
                        }

                        nodePosIdx++;
                        continue;
                    }

                    nodePosition[nodePosIdx] = bits.Length();
                    nodePosIdx++;

                    uint64_t* writeTo = bitOccurences;
                    writeTo[1] = bitOccurences[0] + nodeWritingOffset;
                    writeTo[0] = nodeWritingOffset;
                    for (uint64_t symIdx = readBuffer->nodesPositions[nodeIdx]; symIdx < upperBound; symIdx++) {
                        char symbol = readBuffer->string[symIdx];
                        uint8_t index = *reinterpret_cast<uint8_t*>(&symbol);
                        uint8_t bit = symbolBit[index];

                        bits.AddBit(bit);
                        writeBuffer->string[writeTo[bit]] = symbol;
                        writeTo[bit]++;
                    }

                    nodeWritingOffset = writeTo[1];
                }

                Buffer* temp = writeBuffer;
                writeBuffer = readBuffer;
                readBuffer = temp;
            }

            delete[] stringBuffers[0];
            delete[] stringBuffers[1];

            return nodePosIdx;
        }

        void ComputeNodesRanks() {
            for (uint64_t node = 0; node < nodesCount; node++) {
                for (uint8_t bit = 0; bit < 2; bit++) {
                    nodeRank[bit][node] = rrrString->Rank(bit, nodePosition[node]);

                    if ((*rrrString)[nodePosition[node]] != bit) {
                        nodeRank[bit][node]++;
                    }
                }
            }
        }
    };
}