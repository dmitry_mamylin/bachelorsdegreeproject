﻿#pragma once

#include <cstdint>
#include <vector>

namespace Utility {
    /* Считает биномиальные коэффициенты в разложении (a + b)^power
    и сохраняет их в coefficients */
    void ComputeBynomialCoefficients(std::vector<uint64_t>& coefficients, uint32_t power) {
        coefficients.push_back(1ui64);
        for (auto step = 0ui32; step < power; step++) {
            auto prev = coefficients[0];
            coefficients[0] = 1ui64;

            for (auto elementIdx = 1ui32; elementIdx <= coefficients.size(); elementIdx++) {
                if (elementIdx == coefficients.size()) {
                    coefficients.push_back(1ui64);
                    break;
                }
                else {
                    auto temp = coefficients[elementIdx];
                    coefficients[elementIdx] += prev;
                    prev = temp;
                }
            }
        }
    }
}