#pragma once

#include <cinttypes>
#include <string>

#include "BitField.h"

namespace Utility {
    /* ������� ���������� ������ ��� �������� ������ str � ��������� ��� � ������ sa.
    n = str.Length()
    n+1 = ����� ������ str � ����������� � ����� "$"
    sa - ������ ����� n+1
    prm - �������� � ������� sa: prm[j] = i <=> sa[i] = j*/
    void CreateSuffixArray(const BitField& str, uint64_t* sa, uint64_t* prm) {
        BitField bh, b2h;
        uint64_t* next;
        uint64_t* count;
        const uint64_t n = str.Length();

        next = new uint64_t[2 * (n + 1)];
        count = next + n + 1;

        bh.AllocateZeros(n + 1);
        b2h.AllocateZeros(n + 1);

        /* ��������� �������������� ������ ����������
        (������ ��� "$", ��������� - � ������� �������� �����) */
        uint64_t bitCounts[2] = { 0, 0 }; // ����������� ������� ���������� ����� � ������
        for (uint64_t i = 0; i < n; i++) {
            next[i + 1] = i;
            bitCounts[str[i]]++; // �� ����������� ������ ���������� �����
        }

        // ���������� �� ������� ����:
        sa[0] = n; // ����� ��������� ������� "$", �.�. �� ����� �������
        uint64_t nextPosition[2] = { 1, bitCounts[0] + 1 }; // �������, ���� ���������� �������
        for (uint64_t i = 1; i < n + 1; i++) {
            uint8_t firstBit = str[next[i]];
            sa[nextPosition[firstBit]] = next[i];
            nextPosition[firstBit]++;
            next[i] = 0;
        }

        // �������� ����� ��������� �������-�������:
        bh.WriteBit(0, 1);
        b2h.WriteBit(0, 0);
        bh.WriteBit(1, 1);
        b2h.WriteBit(1, 0);
        for (uint64_t i = 2; i < n + 1; i++) {
            bh.WriteBit(i, str[sa[i]] != str[sa[i - 1]]);
            b2h.WriteBit(i, 0);
        }

        for (uint64_t h = 1; h < n + 1; h <<= 1) {
            uint64_t bucketsCount = 0;

            for (uint64_t i = 0, j; i < n + 1; i = j) {
                j = i + 1;
                while (j < n + 1 && !bh[j]) j++;

                next[i] = j;
                bucketsCount++;
            }
            if (bucketsCount == n + 1) {
                break;
            }

            for (uint64_t i = 0; i < n + 1; i = next[i]) {
                count[i] = 0;
                for (uint64_t j = i; j < next[i]; j++) {
                    prm[sa[j]] = i;
                }
            }

            count[prm[n + 1 - h]]++;
            b2h.WriteBit(prm[n + 1 - h], 1);
            for (uint64_t i = 0; i < n + 1; i = next[i]) {
                for (uint64_t j = i; j < next[i]; j++) {
                    int64_t s = int64_t(sa[j]) - int64_t(h);

                    if (s >= 0) {
                        uint64_t head = prm[s];
                        prm[s] = head + count[head];
                        count[head]++;
                        b2h.WriteBit(prm[s], 1);
                    }
                }

                for (uint64_t j = i; j < next[i]; j++) {
                    int64_t s = int64_t(sa[j]) - int64_t(h);

                    if (s >= 0 && b2h[prm[s]]) {
                        for (uint64_t k = prm[s] + 1; !bh.ReadBit(k) && b2h.ReadBit(k); k++) {
                            b2h.WriteBit(k, 0);
                        }
                    }
                }
            }

            for (uint64_t i = 0; i < n + 1; i++) {
                sa[prm[i]] = i;
                bh.WriteBit(i, bh[i] | b2h[i]);
            }
        }

        delete[] next;
    }

    /* ������� ���������� ������ ��� ������������ ������ str � ��������� ��� � ������ sa.
    n = str.Length()
    sa - ������ ����� n
    prm - �������� � ������� sa: prm[j] = i <=> sa[i] = j*/
    template<class STR_CLASS>
    void CreateSuffixArray(const STR_CLASS& str, uint64_t* sa, uint64_t* prm) {
        BitField bh; // bt[i] == 1 <=> sa[i] - ����� ����� ������� bucket'� ������� h
        BitField b2h;
        uint64_t* next;
        uint64_t* count;
        const uint64_t n = str.length();
        uint64_t charOcc[256];

        next = new uint64_t[2 * n];
        count = next + n;

        bh.AllocateZeros(n);
        b2h.AllocateZeros(n);

        memset(charOcc, 0, sizeof(uint64_t) * 256);

        // ��������� �������������� ������ ����������
        for (uint64_t i = 0; i < n; i++) {
            next[i] = i;
            char sym = str[i];
            charOcc[*reinterpret_cast<const uint8_t*>(&sym)]++; // ��������� ���������� ��������� ��������
        }

        // ������������ ������ charOcc � ������ �������, ���� ����� ������� ��������� �������
        uint64_t* nextPosition = charOcc;
        uint64_t accum = charOcc[0];
        nextPosition[0] = 0;
        for (uint64_t i = 1; i < 256; i++) {
            uint64_t currentCharOcc = charOcc[i];

            nextPosition[i] = accum;
            accum += currentCharOcc;
        }

        // ���������� �� ������� �������:
        for (uint64_t i = 0; i < n; i++) {
            char firstChar = str[next[i]];

            sa[nextPosition[*reinterpret_cast<uint8_t*>(&firstChar)]] = next[i];
            nextPosition[*reinterpret_cast<uint8_t*>(&firstChar)]++;
            next[i] = 0;
        }

        // �������� ����� ��������� �������-�������:
        bh.WriteBit(0, 1);
        b2h.WriteBit(0, 0);
        for (uint64_t i = 1; i < n; i++) {
            bh.WriteBit(i, str[sa[i]] != str[sa[i - 1]]);
            b2h.WriteBit(i, 0);
        }

        for (uint64_t h = 1; h < n; h <<= 1) {
            uint64_t bucketsCount = 0;

            for (uint64_t i = 0, j; i < n; i = j) {
                j = i + 1;
                while (j < n && !bh[j]) j++;

                next[i] = j;
                bucketsCount++;
            }
            if (bucketsCount == n) {
                break;
            }

            for (uint64_t i = 0; i < n; i = next[i]) {
                count[i] = 0;
                for (uint64_t j = i; j < next[i]; j++) {
                    prm[sa[j]] = i;
                }
            }

            count[prm[n - h]]++;
            b2h.WriteBit(prm[n - h], 1);
            for (uint64_t i = 0; i < n; i = next[i]) {
                for (uint64_t j = i; j < next[i]; j++) {
                    int64_t s = int64_t(sa[j]) - int64_t(h);

                    if (s >= 0) {
                        uint64_t head = prm[s];
                        prm[s] = head + count[head];
                        count[head]++;
                        b2h.WriteBit(prm[s], 1);
                    }
                }

                for (uint64_t j = i; j < next[i]; j++) {
                    int64_t s = int64_t(sa[j]) - int64_t(h);

                    if (s >= 0 && b2h[prm[s]]) {
                        for (uint64_t k = prm[s] + 1; !bh.ReadBit(k) && b2h.ReadBit(k); k++) {
                            b2h.WriteBit(k, 0);
                        }
                    }
                }
            }

            for (uint64_t i = 0; i < n; i++) {
                sa[prm[i]] = i;
                bh.WriteBit(i, bh[i] | b2h[i]);
            }
        }

        delete[] next;
    }

    /* ������� ���������� ������ ��� �������� ������ str � ��������� ��� � ������ sa.
    n = str.Length()
    n+1 = ����� ������ str � ����������� � ����� "$"
    sa - ������ ����� n+1
    prm - �������� � ������� sa: prm[j] = i <=> sa[i] = j*/
    void CreateSuffixArray(const BitField& str, uint32_t* sa, uint32_t* prm) {
        BitField bh, b2h;
        uint32_t* next;
        uint32_t* count;
        const uint64_t n = str.Length();

        next = new uint32_t[2 * (n + 1)];
        count = next + n + 1;

        bh.AllocateZeros(n + 1);
        b2h.AllocateZeros(n + 1);

        /* ��������� �������������� ������ ����������
        (������ ��� "$", ��������� - � ������� �������� �����) */
        uint64_t bitCounts[2] = { 0, 0 }; // ����������� ������� ���������� ����� � ������
        for (uint64_t i = 0; i < n; i++) {
            next[i + 1] = uint32_t(i);
            bitCounts[str[i]]++; // �� ����������� ������ ���������� �����
        }

        // ���������� �� ������� ����:
        sa[0] = uint32_t(n); // ����� ��������� ������� "$", �.�. �� ����� �������
        uint64_t nextPosition[2] = { 1, bitCounts[0] + 1 }; // �������, ���� ���������� �������
        for (uint64_t i = 1; i < n + 1; i++) {
            uint8_t firstBit = str[next[i]];
            sa[nextPosition[firstBit]] = next[i];
            nextPosition[firstBit]++;
            next[i] = 0;
        }

        // �������� ����� ��������� �������-�������:
        bh.WriteBit(0, 1);
        b2h.WriteBit(0, 0);
        bh.WriteBit(1, 1);
        b2h.WriteBit(1, 0);
        for (uint64_t i = 2; i < n + 1; i++) {
            bh.WriteBit(i, str[sa[i]] != str[sa[i - 1]]);
            b2h.WriteBit(i, 0);
        }

        for (uint64_t h = 1; h < n + 1; h <<= 1) {
            uint64_t bucketsCount = 0;

            for (uint64_t i = 0, j; i < n + 1; i = j) {
                j = i + 1;
                while (j < n + 1 && !bh[j]) j++;

                next[i] = uint32_t(j);
                bucketsCount++;
            }
            if (bucketsCount == n + 1) {
                break;
            }

            for (uint64_t i = 0; i < n + 1; i = next[i]) {
                count[i] = 0;
                for (uint64_t j = i; j < next[i]; j++) {
                    prm[sa[j]] = uint32_t(i);
                }
            }

            count[prm[n + 1 - h]]++;
            b2h.WriteBit(prm[n + 1 - h], 1);
            for (uint64_t i = 0; i < n + 1; i = next[i]) {
                for (uint64_t j = i; j < next[i]; j++) {
                    int64_t s = int64_t(sa[j]) - int64_t(h);

                    if (s >= 0) {
                        uint32_t head = prm[s];
                        prm[s] = head + count[head];
                        count[head]++;
                        b2h.WriteBit(prm[s], 1);
                    }
                }

                for (uint64_t j = i; j < next[i]; j++) {
                    int64_t s = int64_t(sa[j]) - int64_t(h);

                    if (s >= 0 && b2h[prm[s]]) {
                        for (uint64_t k = prm[s] + 1; !bh.ReadBit(k) && b2h.ReadBit(k); k++) {
                            b2h.WriteBit(k, 0);
                        }
                    }
                }
            }

            for (uint64_t i = 0; i < n + 1; i++) {
                sa[prm[i]] = uint32_t(i);
                bh.WriteBit(i, bh[i] | b2h[i]);
            }
        }

        delete[] next;
    }

    /* ������� ���������� ������ ��� ������������ ������ str � ��������� ��� � ������ sa.
    n = str.Length()
    sa - ������ ����� n
    prm - �������� � ������� sa: prm[j] = i <=> sa[i] = j*/
    void CreateSuffixArray(const std::string& str, uint32_t* sa, uint32_t* prm) {
        BitField bh; // bt[i] == 1 <=> sa[i] - ����� ����� ������� bucket'� ������� h
        BitField b2h;
        uint32_t* next;
        uint32_t* count;
        const uint64_t n = str.length();
        uint32_t charOcc[256];

        next = new uint32_t[2 * n];
        count = next + n;

        bh.AllocateZeros(n);
        b2h.AllocateZeros(n);

        memset(charOcc, 0, sizeof(uint32_t) * 256);

        // ��������� �������������� ������ ����������
        for (uint64_t i = 0; i < n; i++) {
            next[i] = uint32_t(i);
            charOcc[*reinterpret_cast<const uint8_t*>(&str[i])]++; // ��������� ���������� ��������� ��������
        }

        // ������������ ������ charOcc � ������ �������, ���� ����� ������� ��������� �������
        uint32_t* nextPosition = charOcc;
        uint32_t accum = charOcc[0];
        nextPosition[0] = 0;
        for (uint64_t i = 1; i < 256; i++) {
            uint32_t currentCharOcc = charOcc[i];

            nextPosition[i] = accum;
            accum += currentCharOcc;
        }

        // ���������� �� ������� �������:
        for (uint64_t i = 0; i < n; i++) {
            char firstChar = str[next[i]];

            sa[nextPosition[firstChar]] = next[i];
            nextPosition[firstChar]++;
            next[i] = 0;
        }

        // �������� ����� ��������� �������-�������:
        bh.WriteBit(0, 1);
        b2h.WriteBit(0, 0);
        for (uint64_t i = 1; i < n; i++) {
            bh.WriteBit(i, str[sa[i]] != str[sa[i - 1]]);
            b2h.WriteBit(i, 0);
        }

        for (uint64_t h = 1; h < n; h <<= 1) {
            uint64_t bucketsCount = 0;

            for (uint64_t i = 0, j; i < n; i = j) {
                j = i + 1;
                while (j < n && !bh[j]) j++;

                next[i] = uint32_t(j);
                bucketsCount++;
            }
            if (bucketsCount == n) {
                break;
            }

            for (uint64_t i = 0; i < n; i = next[i]) {
                count[i] = 0;
                for (uint64_t j = i; j < next[i]; j++) {
                    prm[sa[j]] = uint32_t(i);
                }
            }

            count[prm[n - h]]++;
            b2h.WriteBit(prm[n - h], 1);
            for (uint64_t i = 0; i < n; i = next[i]) {
                for (uint64_t j = i; j < next[i]; j++) {
                    int64_t s = int64_t(sa[j]) - int64_t(h);

                    if (s >= 0) {
                        uint32_t head = prm[s];
                        prm[s] = head + count[head];
                        count[head]++;
                        b2h.WriteBit(prm[s], 1);
                    }
                }

                for (uint64_t j = i; j < next[i]; j++) {
                    int64_t s = int64_t(sa[j]) - int64_t(h);

                    if (s >= 0 && b2h[prm[s]]) {
                        for (uint64_t k = prm[s] + 1; !bh.ReadBit(k) && b2h.ReadBit(k); k++) {
                            b2h.WriteBit(k, 0);
                        }
                    }
                }
            }

            for (uint64_t i = 0; i < n; i++) {
                sa[prm[i]] = uint32_t(i);
                bh.WriteBit(i, bh[i] | b2h[i]);
            }
        }

        delete[] next;
    }
}