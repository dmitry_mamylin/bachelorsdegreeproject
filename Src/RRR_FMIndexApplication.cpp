#include "RRR_FMIndex/RRR_FMIndex.h"

#include "FileUtilities.h"
#include "StringUtilities.h"

#include <cstdio>
#include <string>
#include <cstring>
#include <vector>
#include <cstdint>
#include <chrono>

int main(int argc, char** argv) {
    if (argc <= 2) {
        puts("Usage: program <inputFile> <samplesFile>");
        puts("Where:");
        puts("inputFile - filename of an input file which will be used to construct the index");
        puts("samplesFile - file that stores samples line-by-line which wil be located in the index");

        return 0;
    }

    std::string* file = new std::string(Utility::ReadFile(argv[1]));

    if (file->empty()) {
        printf("Cannot open a file: ");
        puts(argv[1]);
        return 1;
    }

    uint64_t fileLength = file->length();

    puts("Constructing the index...");
    auto start = std::chrono::high_resolution_clock::now();
    RRR_FMIndex::RRR_FMIndex fmi(*file);
    auto end = std::chrono::high_resolution_clock::now();
    auto diff = end - start;
    puts("Done!");

    delete file;
    file = nullptr;

    printf("File: %s\n", argv[1]);
    printf("Raw file size: %lf MB\n", fileLength / 1048576.0);
    printf("Index size: %lf MB\n", fmi.SizeMB());
    printf("Construction time: %lf minutes\n", (std::chrono::duration<double, std::milli>(diff).count()) / 60000.0);

    std::ifstream samples(argv[2]);
    std::string sample;
    std::vector<uint64_t> occs;
    auto totalQueryTime = std::chrono::duration<long long, std::nano>::zero();

    sample.reserve(1000);
    occs.reserve(1000);
    
    printf("Using samples from %s\n", argv[2]);
    puts("Performing a serie of queries...");
    while(true) {
        std::getline(samples, sample);

        if (samples.eof()) {
            break;
        }

        start = std::chrono::high_resolution_clock::now();
        fmi.LocateOccurences(sample, occs);
        end = std::chrono::high_resolution_clock::now();
        diff = end - start;
        totalQueryTime += diff;

        occs.clear();
        samples.clear();
    }
    puts("Done!");

    printf("Total query time: %lf minutes\n", (std::chrono::duration<double, std::milli>(totalQueryTime).count()) / 60000.0);

    samples.close();

    puts("***************************************************");

    return 0;
}