﻿#pragma once

#include <cstdint>
#include <vector>

namespace RRR {
    namespace DecodeTable {
        template<uint64_t BLOCK_SIZE>
        struct Decode {
            // Возвращает количество байтов, необходимое для хранения всей таблицы Decode
            uint64_t SizeInBytes() const {
                return AddressArraySizeBytes() + TableSizeBytes();
            }

            /* Принимает уже выделенный блок памяти data размером SizeInBytes() байтов
            и размечает его, чтобы хранить address и table */
            void LinkMemory(void* data) {
                address = (uint32_t*)data;
                table = (uint8_t*)&address[BLOCK_SIZE];
            }

            /* Заполняет память.
            classes[] - массив длины CLASSES_COUNT, каждый его элемент - соответствующий класс */
            void Initialize(const std::vector<uint16_t>* classes) {
                uint32_t addressIdx = 0;
                uint32_t addressValue = 0;
                for (uint64_t classIdx = 0; classIdx < CLASSES_COUNT; classIdx++) {
                    address[addressIdx] = addressValue;
                    addressIdx++;

                    for (uint16_t element : classes[classIdx]) {
                        // Записываем каждый элемент:
                        ((uint16_t*)(&table[addressValue]))[0] = element;
                        addressValue += 2;

                        // Считаем rank1 для каждого элемента и записываем его:
                        uint8_t rank1 = 0;
                        for (int32_t shift = BLOCK_SIZE - 1; shift >= 0; shift--) {
                            uint8_t bit = uint8_t(element >> shift) & uint8_t(1);
                            rank1 += bit;

                            table[addressValue] = rank1;

                            addressValue++;
                        }
                    }
                }
            }

            /* classIdx = 0, 1, ..., CLASSES_COUNT - 1
            elementIdx = 0, 1, ...
            bitIdx = 0, 1, ..., 15
            Возвращает rank1 бита bitIdx элемента с индексом elementIdx в классе номер classIdx */
            uint8_t Rank1(uint32_t classIdx, uint32_t elementIdx, uint8_t bitIdx) const {
                return table[address[classIdx] + 2 + (2 + BLOCK_SIZE)*elementIdx + bitIdx];
            }

            /* classIdx = 0, 1, ..., CLASSES_COUNT - 1
            elementIdx = 0, 1, ...
            bitIdx = 0, 1, ..., 15
            Возвращает элемент с индексом elementIdx в классе номер classIdx */
            uint16_t Element(uint32_t classIdx, uint32_t elementIdx) const {
                return ((uint16_t*)(&table[address[classIdx] + (2 + BLOCK_SIZE)*elementIdx]))[0];
            }

        private:
            /* Количество классов. Подразумеваем, что элементы 0...00 и 1...11
            хранятся в одном классе. */
            static constexpr uint32_t CLASSES_COUNT = BLOCK_SIZE;

            /* address[i] хранит позицию данных первого элемента i-го класса в массиве table.
            При BLOCK_SIZE = 16, address занимает не более 64 байтов памяти */
            uint32_t* address = nullptr;

            /* table[i] хранит все элементы всех классов и их значения rank1.
            При BLOCK_SIZE = 16, table занимает не более 1.2 MB памяти */
            uint8_t* table = nullptr;

            // Возвращает количество байтов, необходимое для хранения массива address
            uint64_t AddressArraySizeBytes() const {
                uint64_t bytesPerAddress = sizeof(uint32_t);

                return CLASSES_COUNT * bytesPerAddress;
            }

            // Возвращает количество байтов, необходимое для хранения массива table
            uint64_t TableSizeBytes() const {
                // Всего 2^BLOCK_SIZE элементов:
                uint64_t elementsCount = uint64_t(1) << BLOCK_SIZE;

                // Ранг каждой позиции каждого элемента требует для хранения 1 байт:
                uint64_t bytesPerEachRank = sizeof(uint8_t);

                // Количество байтов для хранения каждого элемента
                uint64_t bytesPerElement = sizeof(uint16_t);

                // Количество байтов для хранения всех рангов одного элемента
                uint64_t bytesPerElementRanks = BLOCK_SIZE * bytesPerEachRank;

                return elementsCount * (bytesPerElement + bytesPerElementRanks);
            }
        };
    }
}