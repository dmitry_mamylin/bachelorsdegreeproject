﻿#pragma once

#include <cinttypes>
#include <string>
#include <vector>

#include "../BynomialUtilities.h"
#include "../Samples.h"
#include "../BitField.h"

#include "StringWrapper.h"
#include "Decode.h"
#include "PackedString.h"

/*Всего BLOCK_SIZE + 1 класс, которым соответствуют индексы l = 0, 1, ..., BLOCK_SIZE.
В каждом классе C(BLOCK_SIZE, 0), C(BLOCK_SIZE, 1), ..., C(BLOCK_SIZE, BLOCK_SIZE) элементов соответственно.
Заметим, что если BLOCK_SIZE = 2^k, то всего k+1 класс, тогда для хранения индексов классов понадобится:
ceil(log2(1 + 2^k)) = k+1 битов, но если объединим блоки 00...0 и 11...1 в один класс (нулевой), тогда для хранения
индексов потребуется ровно k битов.*/
namespace RRR {
    class RRRSuccinctBinaryString {
    public:
        static constexpr uint64_t SUPERBLOCK_SIZE = 256;
        static constexpr uint64_t BITS_PER_CLASS_IDX = 4;
        static constexpr uint64_t BLOCK_SIZE = 1 << BITS_PER_CLASS_IDX;
        static constexpr uint64_t CLASSES_COUNT = BLOCK_SIZE;
        static constexpr uint64_t ELEMENTS_COUNT = 1 << BLOCK_SIZE;

        static constexpr uint64_t BLOCKS_IN_SUPERBLOCK = SUPERBLOCK_SIZE / BLOCK_SIZE;

        RRRSuccinctBinaryString(const std::string& bits) {
            RRR::Wrapper::StdString wrappedBits(bits);
            Initialize(wrappedBits);
        }

        RRRSuccinctBinaryString(const Utility::BitField& bits) {
            RRR::Wrapper::BitFieldString wrappedBits(bits);
            Initialize(wrappedBits);
        }

        uint64_t Rank(const uint8_t bit, const uint64_t i) const {
            return (this->*rank[bit])(i);
        }

        // i = 0, 1, ..., n-1
        uint64_t Rank1(const uint64_t i) const {
            uint64_t blockIdx = i / BLOCK_SIZE;
            uint64_t superblockIdx = i / SUPERBLOCK_SIZE;
            uint8_t classIdx;
            uint16_t elementIdx;

            packedString.GetBlockData(blockIdx, classIdx, elementIdx);

            return superblockrank[superblockIdx] +
                blockrank[blockIdx] +
                decode.Rank1(classIdx, elementIdx, i % BLOCK_SIZE);
        }

        // i = 0, 1, ..., n-1
        uint64_t Rank0(const uint64_t i) const {
            return (i + 1) - Rank1(i);
        }

        double DecodeTableSizeMB() const {
            return decode.SizeInBytes() / 1048576.0;
        }

        double PackedStringSizeMB() const {
            return packedString.SizeInBytes() / 1048576.0;
        }

        double RankStructureSizeMB() const {
            return bytesPerRankStructure / 1048576.0;
        }

        double TotalSizeMB() const {
            return DecodeTableSizeMB() +
                PackedStringSizeMB() +
                RankStructureSizeMB();
        }

        uint8_t operator[](const uint64_t i) const {
            uint64_t blockIdx = i / BLOCK_SIZE;
            uint8_t bitIdx = uint8_t(i % BLOCK_SIZE);
            uint8_t classIdx;
            uint16_t elementIdx;

            packedString.GetBlockData(blockIdx, classIdx, elementIdx);
            uint16_t block = decode.Element(classIdx, elementIdx);

            return uint8_t(block >> (BLOCK_SIZE - 1 - bitIdx)) & uint8_t(1);
            //return char('0' + (uint8_t(block >> (BLOCK_SIZE - 1 - bitIdx)) & uint8_t(1)));
        }

        ~RRRSuccinctBinaryString() {
            if (data) {
                free(data);
                data = nullptr;
            }
        }

    private:
        typedef uint64_t(RRRSuccinctBinaryString::*rankFuncPtr)(const uint64_t) const;
        rankFuncPtr rank[2] = { &RRRSuccinctBinaryString::Rank0, &RRRSuccinctBinaryString::Rank1 };

        void* data = nullptr;

        DecodeTable::Decode<BLOCK_SIZE> decode;
        Utility::Samples<uint64_t, SUPERBLOCK_SIZE> superblockrank;
        Utility::Samples<uint8_t, BLOCK_SIZE> blockrank;
        PackedString::PackedString<BITS_PER_CLASS_IDX, BLOCK_SIZE> packedString;

        uint64_t bytesPerRankStructure;

        void AllocateMemory(const uint64_t strLen) {
            // Подсчитываем количество байтов, необходимое для хранения
            uint64_t bytesPerDecode = decode.SizeInBytes();
            uint64_t bytesPerSuperblockrank = superblockrank.SizeInBytes(strLen);
            uint64_t bytesPerBlockrank = blockrank.SizeInBytes(strLen);
            uint64_t bytesPerPackedString = packedString.SizeInBytes();

            bytesPerRankStructure = bytesPerBlockrank + bytesPerSuperblockrank;

            uint64_t bytesTotal =
                bytesPerDecode +
                bytesPerSuperblockrank +
                bytesPerBlockrank +
                bytesPerPackedString;

            // Выделяем память и связываем ее со структурами:
            data = malloc(bytesTotal);
            uint8_t* allocatedBytes = (uint8_t*)data;

            decode.LinkMemory((void*)allocatedBytes);
            allocatedBytes += bytesPerDecode;

            superblockrank.LinkMemory((void*)allocatedBytes);
            allocatedBytes += bytesPerSuperblockrank;

            blockrank.LinkMemory((void*)allocatedBytes);
            allocatedBytes += bytesPerBlockrank;

            packedString.LinkMemory((void*)allocatedBytes);
        }

        void Initialize(const RRR::Wrapper::StringWrapper& bits) {
            std::vector<uint16_t> classes[CLASSES_COUNT]; // Для decode
            std::vector<uint8_t> bitsPerElementIdx; // Для packedString
            std::vector<uint8_t> classesIndicesSequence; // Для packedString
            std::vector<uint16_t> elementsIndicesSequence; // Для packedString
            std::vector<uint64_t> explicitRank1Samples;
            std::vector<uint16_t> indexOfElement; // Хранит локальный индекс элемента в своем классе

            explicitRank1Samples.reserve(blockrank.SamplesCount(bits.length()));
            classesIndicesSequence.reserve(bits.length() / BLOCK_SIZE);
            elementsIndicesSequence.reserve(bits.length() / BLOCK_SIZE);
            indexOfElement.reserve(ELEMENTS_COUNT);

            ComputeBitsPerClass(bitsPerElementIdx);
            ComputeAllClasses(classes, indexOfElement);

            // Вычисляем количество битов для сжатой строки и считаем rank1:
            uint64_t bitsPerClassesIndices = 0;
            uint64_t bitsPerElementsIndices = 0;
            uint64_t rank1 = 0;
            const uint64_t upperBound = BLOCK_SIZE * (bits.length() / BLOCK_SIZE);
            for (uint64_t charIdx = 0; charIdx < upperBound; charIdx += BLOCK_SIZE) {
                uint8_t classIdx = 0;
                uint16_t element = 0;

                explicitRank1Samples.push_back(rank1);

                for (uint64_t i = 0; i < BLOCK_SIZE; i++) {
                    uint64_t add = uint64_t(bits[i + charIdx] - '0') & uint64_t(1);
                    classIdx += uint8_t(add);
                    rank1 += add;

                    element = (element << 1) | uint16_t(add);
                }
                classIdx %= CLASSES_COUNT;

                bitsPerClassesIndices += BITS_PER_CLASS_IDX;
                bitsPerElementsIndices += bitsPerElementIdx[classIdx];

                classesIndicesSequence.push_back(classIdx);
                elementsIndicesSequence.push_back(indexOfElement[element]);
            }

            // Если длина строки не кратна длине блока (BLOCK_SIZE), то считываем остаток строки:
            if (upperBound < bits.length()) {
                uint8_t classIdx = 0;
                uint16_t element = 0;
                explicitRank1Samples.push_back(rank1);

                for (uint64_t charIdx = upperBound; charIdx < upperBound + BLOCK_SIZE; charIdx++) {
                    element <<= 1;
                    if (charIdx < bits.length()) {
                        uint64_t add = uint64_t(bits[charIdx] - '0');
                        classIdx += uint8_t(add);
                        element |= uint16_t(add);
                    }
                }
                classIdx %= CLASSES_COUNT;

                bitsPerElementsIndices += bitsPerElementIdx[classIdx];
                bitsPerClassesIndices += BITS_PER_CLASS_IDX;

                classesIndicesSequence.push_back(classIdx);
                elementsIndicesSequence.push_back(indexOfElement[element]);
            }

            packedString.ComputeSize(bitsPerClassesIndices, bitsPerElementsIndices);

            AllocateMemory((uint64_t)bits.length());
            FillMemory(
                classes,
                bitsPerElementIdx,
                classesIndicesSequence,
                elementsIndicesSequence,
                explicitRank1Samples,
                bits);
        }

        void FillMemory(
            const std::vector<uint16_t>* classes,
            const std::vector<uint8_t>& bitsPerElementIdx,
            const std::vector<uint8_t>& classesIndicesSequence,
            const std::vector<uint16_t>& elementsIndicesSequence,
            const std::vector<uint64_t>& explicitRank1Samples,
            const RRR::Wrapper::StringWrapper& bits)
        {
            decode.Initialize(classes);
            packedString.Initialize(bitsPerElementIdx, classesIndicesSequence, elementsIndicesSequence);

            uint64_t superblockIdx = 0;
            const uint64_t samplesCount = blockrank.SamplesCount(bits.length());
            for (size_t i = 0; i < samplesCount; i++) {
                uint64_t bitIdx = BLOCK_SIZE * i;

                if (bitIdx % SUPERBLOCK_SIZE == 0) {
                    superblockrank[superblockIdx] = explicitRank1Samples[i];
                    superblockIdx++;
                }

                blockrank[(uint64_t)i] = uint8_t(explicitRank1Samples[i] - superblockrank[superblockIdx - 1]);
            }
        }

        /* Распределяет все элементы длины BLOCK_SIZE по своим классам
        classes - массив длины CLASSES_COUNT
        indexOfElement - массив длины ELEMENTS_COUNT, classOfElement[element] хранит локальный индекс
        элемента element в его классе */
        static void ComputeAllClasses(std::vector<uint16_t>* classes, std::vector<uint16_t>& indexOfElement) {
            uint16_t elementsWritten[CLASSES_COUNT];

            for (uint64_t i = 0; i < CLASSES_COUNT; i++) {
                elementsWritten[i] = 0;
            }

            for (uint64_t element = 0; element < ELEMENTS_COUNT; element++) {
                uint8_t classIdx = GetClassIdx((uint16_t)element);

                classes[classIdx].push_back((uint16_t)element);
                indexOfElement.push_back(elementsWritten[classIdx]);

                elementsWritten[classIdx]++;
            }
        }

        /* Считает количество единиц в элементе. Это - индекс класса.
        Также, если BLOCK_SIZE единиц, то считается, что это класс с индексом 0 */
        static uint8_t GetClassIdx(uint16_t element) {
            uint8_t classIdx = 0;
            for (int64_t shift = BLOCK_SIZE - 1; shift >= 0; shift--) {
                classIdx += uint8_t(element >> shift) & uint8_t(1);
            }

            return classIdx % BLOCK_SIZE;
        }

        /* Вычисляет количество битов для хранения индексов элементов класса.
        bitsPerElementIdx[i] хранит количество битов, необходимое для хранения
        индекса одного элемента в i-ом классе */
        static void ComputeBitsPerClass(std::vector<uint8_t>& bitsPerElementIdx) {
            std::vector<uint64_t> bynomialCoeff;
            bynomialCoeff.reserve(CLASSES_COUNT + 1);
            Utility::ComputeBynomialCoefficients(bynomialCoeff, CLASSES_COUNT);

            bitsPerElementIdx.push_back(1); // Для 0-го класса (хранит 0..00 и 1..11)
            for (uint64_t classIdx = 1; classIdx < CLASSES_COUNT; classIdx++) {
                double coeff = double(bynomialCoeff[classIdx]);
                bitsPerElementIdx.push_back((uint8_t)ceil(log2(coeff)));
            }
        }
    };
}