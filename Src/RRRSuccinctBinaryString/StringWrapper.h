﻿#pragma once

#include <cstdint>
#include <string>

#include "../BitField.h"

namespace RRR {
    namespace Wrapper {
        class StringWrapper {
        public:
            StringWrapper() {}
            virtual ~StringWrapper() {}

            virtual size_t length() const = 0;
            virtual char operator[](const size_t i) const = 0;
        };

        class StdString : public StringWrapper {
        private:
            const std::string& bits;

        public:
            StdString(const std::string& string) :
                bits(string) {}

            size_t length() const override {
                return bits.length();
            }

            char operator[](const size_t i) const override {
                return bits[i];
            }
        };

        class BitFieldString : public StringWrapper {
        private:
            const Utility::BitField& bits;

        public:
            BitFieldString(const Utility::BitField& bitField) :
                bits(bitField) {}

            size_t length() const override {
                return (size_t)bits.Length();
            }

            char operator[](const size_t i) const override {
                return char('0' + bits[i]);
            }
        };
    }
}