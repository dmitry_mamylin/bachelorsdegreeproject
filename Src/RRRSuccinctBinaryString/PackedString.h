﻿#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include "../Samples.h"
#include "../BitArray.h"

namespace RRR {
    namespace PackedString {
        template<uint8_t BITS_PER_CLASS_IDX, uint64_t BLOCK_SIZE>
        struct PackedString {
            // Размер суперблока для разбивания позиций массива stringData
            static constexpr uint64_t BLOCKS_SAMPLE_STEP = 32;

            void ComputeSize(uint64_t bitsPerClassesIndices, uint64_t bitsPerElementsIndices) {
                sizeInBytes = sizeof(uint8_t) * CLASSES_COUNT;
                uint64_t blocksCount = bitsPerClassesIndices / BITS_PER_CLASS_IDX;

                classesIndicesSize = classesIndices.SizeInBytes(bitsPerClassesIndices);
                samplesSize = bitSamples.SizeInBytes(blocksCount);

                sizeInBytes += samplesSize;
                sizeInBytes += classesIndicesSize;
                sizeInBytes += elementsIndices.SizeInBytes(bitsPerElementsIndices);
            }

            uint64_t SizeInBytes() const {
                return sizeInBytes;
            }

            void LinkMemory(void* data) {
                uint8_t* bytes = (uint8_t*)data;

                bitsPerClassTable = bytes;
                bytes += sizeof(uint8_t) * CLASSES_COUNT;

                bitSamples.LinkMemory((void*)bytes);
                bytes += samplesSize;

                classesIndices.LinkMemory((void*)bytes);
                bytes += classesIndicesSize;

                elementsIndices.LinkMemory((void*)bytes);
            }

            void Initialize(
                const std::vector<uint8_t>& bitsPerElementIdx,
                const std::vector<uint8_t>& classesIndicesSequence,
                const std::vector<uint16_t>& elementsIndicesSequence)
            {
                for (size_t i = 0; i < bitsPerElementIdx.size(); i++) {
                    bitsPerClassTable[i] = bitsPerElementIdx[i];
                }

                uint64_t bitIdx = 0;
                uint64_t blockIdx = 0;
                for (size_t i = 0; i < classesIndicesSequence.size(); i++) {
                    if (i % BLOCKS_SAMPLE_STEP == 0) {
                        bitSamples[blockIdx] = bitIdx;
                        blockIdx++;
                    }

                    uint64_t classIdx = (uint64_t)classesIndicesSequence[i];
                    uint64_t bitsCount = bitsPerClassTable[classIdx];

                    classesIndices.Write(BITS_PER_CLASS_IDX * i, classIdx, BITS_PER_CLASS_IDX);
                    elementsIndices.Write(bitIdx, elementsIndicesSequence[i], bitsCount);

                    bitIdx += bitsCount;
                }
            }

            void GetBlockData(const uint64_t blockIdx, uint8_t& classIdx, uint16_t& elementIdx) const {
                classIdx = (uint8_t)classesIndices.Read(blockIdx * BITS_PER_CLASS_IDX,
                    (uint64_t)BITS_PER_CLASS_IDX);

                uint64_t samplesLeft = blockIdx % BLOCKS_SAMPLE_STEP;
                uint64_t bitIdx = bitSamples[blockIdx / BLOCKS_SAMPLE_STEP];
                uint64_t idx = blockIdx - samplesLeft;
                while (samplesLeft > 0) {
                    bitIdx += bitsPerClassTable[classesIndices.Read(idx * BITS_PER_CLASS_IDX,
                        BITS_PER_CLASS_IDX)];
                    samplesLeft--;
                    idx++;
                }
                uint64_t bitsToRead = bitsPerClassTable[classesIndices.Read(idx * BITS_PER_CLASS_IDX,
                    BITS_PER_CLASS_IDX)];

                elementIdx = (uint16_t)elementsIndices.Read(bitIdx, bitsToRead);
            }

        private:
            static constexpr uint64_t CLASSES_COUNT = BLOCK_SIZE;

            // Количество байтов, необходимое для структуры
            uint64_t sizeInBytes;

            uint64_t classesIndicesSize;
            uint64_t samplesSize;

            /* Массив длины CLASSES_COUNT.
            bitsPerClassTable[i] хранит количество битов, необходимое для хранения
            индекса одного элемента в i-ом классе */
            uint8_t* bitsPerClassTable = nullptr;

            Utility::Samples<uint64_t, BLOCKS_SAMPLE_STEP> bitSamples;
            Utility::BitArray classesIndices;
            Utility::BitArray elementsIndices;
        };
    }
}