﻿#include "RRR_FMIndex/RRR_FMIndex.h"

#include "StringUtilities.h"

bool DoReadSegmentTestsOnRandomString(const uint64_t length, const uint64_t testsCount) {
    for (uint64_t testIdx = 0; testIdx < testsCount; testIdx++) {
        std::string str = Utility::CreateRandomString(length, 'a', 'z');
        std::string segment;
        RRR_FMIndex::RRR_FMIndex fmi(str);

        segment.reserve(length);

        for (uint64_t s = 0; s < length; s++) {
            for (uint64_t e = s; e < length; e++) {
                segment.clear();

                fmi.ReadSegment(segment, s, e);

                uint64_t i = s;
                for (auto it = segment.rbegin(); it != segment.rend(); ++it) {
                    if (str[i] != *it) {
                        printf("Error reading T[%llu..%llu]\n", s, e);
                        return false;
                    }
                    i++;
                }
            }
        }
    }

    return true;
}

void LocateAllOccurences(const std::string& T, const std::string& P, std::vector<uint64_t>& occs) {
    size_t pos = T.find(P, 0);
    while (pos != std::string::npos)
    {
        occs.push_back(pos);
        pos = T.find(P, pos + 1);
    }
}

bool DoOccurencesLocationTestsOnRandomString(const uint64_t length, const uint64_t testsCount) {
    for (uint64_t testIdx = 0; testIdx < testsCount; testIdx++) {
        std::string T = Utility::CreateRandomString(length, 'a', 'z');
        std::vector<uint64_t> givenOccs;
        std::vector<uint64_t> expectedOccs;
        RRR_FMIndex::RRR_FMIndex fmi(T);

        givenOccs.reserve(length);
        expectedOccs.reserve(length);

        for (uint64_t s = 0; s < length; s++) {
            for (uint64_t e = s; e < length; e++) {
                givenOccs.clear();
                expectedOccs.clear();

                std::string P = T.substr(s, e - s + 1);

                fmi.LocateOccurences(P, givenOccs);
                LocateAllOccurences(T, P, expectedOccs);

                if (givenOccs.size() != expectedOccs.size()) {
                    puts("Occs counts are different");
                    return false;
                }

                for (uint64_t expectedOcc : expectedOccs) {
                    bool found = false;
                    for (uint64_t givenOcc : givenOccs) {
                        if (expectedOcc == givenOcc) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        puts("Occ wasn't found");
                        return false;
                    }
                }
            }
        }
    }

    return true;
}

constexpr uint64_t TESTS_COUNT = 40;
constexpr uint64_t STR_LENGTH = uint64_t(1) << 9;

int main() {
    /*if (!DoReadSegmentTestsOnRandomString(STR_LENGTH, TESTS_COUNT)) {
        puts("Error!");
    } else {
        puts("Success!");
    }*/

    if (!DoOccurencesLocationTestsOnRandomString(STR_LENGTH, TESTS_COUNT)) {
        puts("Error!");
    }
    else {
        puts("Success!");
    }

    /*std::vector<uint64_t> occs;
    RRR_FMIndex::RRR_FMIndex fmi("banana");
    fmi.LocateOccurences("b", occs);
    for (uint64_t occ : occs) {
        printf("%llu\n", occ);
    }*/

    /*std::vector<uint64_t> occs;
    RRR_FMIndex::RRR_FMIndex fmi("wmeq");
    fmi.LocateOccurences("w", occs);
    for (uint64_t occ : occs) {
        printf("%llu\n", occ);
    }*/

    system("pause");

    return 0;
}