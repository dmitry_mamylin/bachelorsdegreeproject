﻿#pragma once

#include <string>

namespace Utility {
    // Строка с виртуально добавленным джокером
    class WildcardedString {
    public:
        class iterator;

        WildcardedString(const std::string& srcStr, const size_t wildcardPos) :
            str(srcStr),
            wildcardPosition(wildcardPos) {}

        size_t length() const {
            return str.length() + 1;
        }

        char operator[](const size_t i) const {
            if (i < wildcardPosition) {
                return str[i];
            } else if (i > wildcardPosition) {
                return str[i + 1];
            }

            return WILDCARD;
        }

        iterator begin() const {
            return iterator(*this, 0);
        }

        iterator end() const {
            return iterator(*this, str.length() + 1);
        }

        static bool IsWildcard(char c) {
            return WILDCARD == c;
        }

        class iterator {
        public:
            iterator(const WildcardedString& wstr, size_t position) :
                str(wstr),
                pos(position) {}

            bool operator!=(const iterator& other) const {
                return pos != other.pos;
            }

            const iterator& operator++() {
                ++pos;
                return *this;
            }

            char operator*() const {
                return str[pos];
            }

        private:
            const WildcardedString& str;
            size_t pos;
        };

    private:
        static constexpr char WILDCARD = char(1);

        size_t wildcardPosition;
        const std::string& str;
    };
}