﻿#pragma once

#include <random>
#include <ctime>
#include <algorithm>
#include <string>

namespace Utility {
    void RandomizeString(std::string& str, uint64_t length, uint8_t minChar, uint8_t maxChar) {
        int minInt = (int)std::min(minChar, maxChar);
        int maxInt = (int)std::max(minChar, maxChar);
        std::default_random_engine engine((unsigned)time(NULL));
        std::uniform_int_distribution<int> distr(minInt, maxInt);

        for (uint64_t i = 0; i < length; i++) {
            uint8_t symbol = distr(engine);
            str += *reinterpret_cast<char*>(&symbol);
        }
    }

    std::string CreateRandomString(const uint64_t length, const uint8_t minChar, const uint8_t maxChar) {
        int minInt = (int)std::min(minChar, maxChar);
        int maxInt = (int)std::max(minChar, maxChar);

        std::default_random_engine engine((unsigned)time(NULL));
        std::uniform_int_distribution<int> distr(minInt, maxInt);
        std::string str;
        str.reserve(length);

        for (uint64_t i = 0; i < length; i++) {
            uint8_t symbol = uint8_t(distr(engine));
            str += *reinterpret_cast<char*>(&symbol);
        }

        return str;
    }
}