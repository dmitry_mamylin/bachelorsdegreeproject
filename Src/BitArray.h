﻿#pragma once

#include <cinttypes>
#include <cstdlib>
#include <cmath>

namespace Utility {
    class BitArray {
    private:
        uint64_t* data;

    public:
        // Возвращает количество байтов, необходимое для хранения массива
        uint64_t SizeInBytes(const uint64_t bitsToStore) const {
            return uint64_t(ceil(bitsToStore / 64.0) * sizeof(uint64_t));
        }

        void LinkMemory(void* data) {
            this->data = (uint64_t*)data;
        }

        /* Считывает count битов начиная с бита под номером index
        count = 1, 2, ..., 64 */
        uint64_t Read(uint64_t index, uint64_t count) const {
            uint64_t blockIdx = index / 64;
            uint64_t bitIdx = index % 64;
            uint64_t value;

            uint64_t firstBlock = data[blockIdx];

            // Количество битов в промежутке [bitIdx, ..., 63]
            uint64_t bitsDiff = 64 - bitIdx;
            if (count <= bitsDiff) {
                // Искомые биты занимают ровно 1 блок: firstBlock
                uint64_t mask = ~uint64_t(0) >> (64 - count);
                uint64_t shift = bitsDiff - count;

                mask <<= shift;
                mask &= firstBlock;
                value = mask >> shift;
            }
            else {
                // Если биты занимают 2 блока, то считываем левую и правую часть
                uint64_t secondBlock = data[blockIdx + 1];

                uint64_t leftPart = ~uint64_t(0) >> (64 - bitsDiff);
                leftPart &= firstBlock;

                uint64_t rightPartSize = count - bitsDiff;
                uint64_t rightPart = (~uint64_t(0) >> (64 - rightPartSize)) << (64 - rightPartSize);
                rightPart &= secondBlock;

                leftPart <<= rightPartSize;
                rightPart >>= 64 - rightPartSize;
                value = leftPart | rightPart;
            }

            return value;
        }

        // Записывает count правых битов переменной value в позицию бита с номером index
        void Write(uint64_t index, uint64_t value, uint64_t count) {
            value &= ~uint64_t(0) >> (64 - count);
            uint64_t blockIdx = index / 64;
            uint64_t bitIdx = index % 64;

            uint64_t firstBlock = data[blockIdx];

            uint64_t bitsDiff = 64 - bitIdx;
            if (count <= bitsDiff) {
                uint64_t mask = ~uint64_t(0) >> (64 - count);
                uint64_t shift = bitsDiff - count;

                value <<= shift;
                firstBlock &= ~(mask << shift);
                firstBlock |= value;

                data[blockIdx] = firstBlock;
            }
            else {
                uint64_t secondBlock = data[blockIdx + 1];

                uint64_t leftPart = ~uint64_t(0) >> (64 - bitsDiff);
                firstBlock &= ~leftPart;

                uint64_t rightPartSize = count - bitsDiff;
                uint64_t rightPart = (~uint64_t(0) >> (64 - rightPartSize)) << (64 - rightPartSize);
                secondBlock &= ~rightPart;

                firstBlock |= value >> rightPartSize;
                data[blockIdx] = firstBlock;

                secondBlock |= value << (64 - rightPartSize);
                data[blockIdx + 1] = secondBlock;
            }
        }
    };
}